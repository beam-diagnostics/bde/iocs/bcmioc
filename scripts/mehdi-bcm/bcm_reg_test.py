#------------------------------------------------------------------------------
#  Project     : BCM
#-------------------------------------------------------------------------------
#  File        : bcm_test.py
#  Authors     : Mehdi Mohammadnezhad
#  Created     : 2018-12-14
#  Last update : 2018-12-14
#  Platform    : SIS8300-KU
#  Standard    :
#-------------------------------------------------------------------------------
#  Description : Test script to test functionality of BCM on
#                the SIS8300-KU board
#  Problems    :
#-------------------------------------------------------------------------------
#  Copyright (c) 2018 European Spallation Source ERIC
#-------------------------------------------------------------------------------
#  Revisions  :
#
#  0.1  : 2018-12-14 Mehdi Mohammadnezhad
#         Created
#-------------------------------------------------------------------------------

import time
import math
import os
import sys
import inspect
import struct

# Load modules from other folders
import_folder = "ess-bcm-struck-sis8300-ku/sw/python/bcm"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import bcm

import_folder = "ess-bcm-struck-sis8300-ku/lib/hw/ess_fpga_framework/sw/python"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import essffw


#-------------------------------------------------------------------------------
# Test procedure
#-------------------------------------------------------------------------------

print()
print("****************************************************")
print(" Test of BCM on SIS8300-KU")
print("****************************************************")
print()

essffw_devices = essffw.find_essffw_devices()
print("BCM ESSFFW-running devices found: {}".format(essffw_devices))
if len(essffw_devices) < 1:
    print("Error: No BCM ESSFFW-running device found in system.")
    exit()

board_under_test = essffw.Device(essffw.SIS8300Drv())
board_under_test.add_register_bank(bcm.bcm_com_registers)
board_under_test.add_register_bank(bcm.bcm_registers)
board_under_test.connect(essffw_devices[0])


for x, y in board_under_test.register_banks.items():
  if(y.name == 'BCM_COM'):
    for register in y.get_registers():  
        print(register.name)





print()
print("Full register map")
print("-----------------")
print()
#board_under_test.print_register_map()

passed_tests = 0
run_tests = 0

#passed_tests += board_under_test.test_registers_after_reset()
