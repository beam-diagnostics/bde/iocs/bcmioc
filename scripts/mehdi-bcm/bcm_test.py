#-------------------------------------------------------------------------------
#  Project     : ESS FPGA Framework
#-------------------------------------------------------------------------------
#  File        : sis8300_test.py
#  Authors     : Christian Amstutz
#  Created     : 2017-10-05
#  Last update : 2018-07-19
#  Platform    : SIS8300-KU
#  Standard    :
#-------------------------------------------------------------------------------
#  Description : Test script to test functionality of the ESS FPGA Framework on
#                the SIS8300-KU board
#  Problems    :
#-------------------------------------------------------------------------------
#  Copyright (c) 2018 European Spallation Source ERIC
#-------------------------------------------------------------------------------
#  Revisions  :
#
#  0.01 : 2018-07-19  Christian Amstutz
#         Created
#-------------------------------------------------------------------------------

import time
import math
import struct
#from PyQt5 import QtGui, uic
#from PyQt5.QtCore import *
#import pyqtgraph as pg
#import matplotlib.pyplot as plt
import essffw

# mem_size = 0x400000000
mem_size       = 0x100000
mem_block_size = 64            # Bytes

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
#-------------------------------------------------------------------------------
# Registers of the test custom logic
#-------------------------------------------------------------------------------
test_cstmlog_registers = {
    'CL_MEM_IF_CTRL'       : essffw.FrameworkReg(0x401, 0x00000000, 'WX' , 0xFFFFFFFF),
    'CL_MEM_WR_BASE_ADDR'  : essffw.FrameworkReg(0x402, 0x00000000, 'RWY', 0xFFFFFFFF),
    'CL_MEM_LD_BASE_ADDR'  : essffw.FrameworkReg(0x403, 0x00000000, 'RWY', 0xFFFFFFFF),
    'CL_SP_ADDR'           : essffw.FrameworkReg(0x404, 0x00000000, 'RW' , 0xFFFFFFFF),
    'CL_FF_ADDR'           : essffw.FrameworkReg(0x405, 0x00000000, 'RW' , 0xFFFFFFFF),
    'CL_ADC_CLK_CNTR'      : essffw.FrameworkReg(0x406, None,       'R'  , 0xFFFFFFFF),
    'CL_DAC_SIGNAL_SELECT' : essffw.FrameworkReg(0x407, 0x00000000, 'RWY', 0xFFFFFFFF),
    'CL_DAC_CH_0_VALUE'    : essffw.FrameworkReg(0x408, 0x00000000, 'RWY', 0xFFFFFFFF),
    'CL_DAC_CH_1_VALUE'    : essffw.FrameworkReg(0x409, 0x00000000, 'RWY', 0xFFFFFFFF)
}

bcm_common_registers = {
   'INT_GATEWARE_VERSION'            : essffw.FrameworkReg(0x400 , 0x68312 , 'R'  , 0xFFFFFFFF),
   'INT_SOFTWARE_VERSION'            : essffw.FrameworkReg(0x401 , 0x0     , 'R'  , 0xFFFFFFFF),
   'INT_BEAM_PULSE_PERIOD_WIDTH_SEL' : essffw.FrameworkReg(0x402 , 0x0     , 'RW' , 0xFFFFFFFF),
   'INT_MIN_TRIG_PERIOD_SRC_SEL'     : essffw.FrameworkReg(0x403 , 0x0     , 'RW' , 0x00000001),
   'INT_RESET_ADC_FIFO'              : essffw.FrameworkReg(0x409 , 0x0     , 'RWY', 0x00000003), 
   'INT_REF_CLK_FRQ_KHZ'             : essffw.FrameworkReg(0x40a , 0x0     , 'RWY', 0x000FFFFF), 
   'INT_REF_CLK_THRSH_KHZ'           : essffw.FrameworkReg(0x40b , 0x0     , 'RWY', 0x000FFFFF), 
   'BUILD_TIMESTAMP'                 : essffw.FrameworkReg(0x40c , 0x0     , 'R'  , 0xFFFFFFFF),
   'BUILD_VERSION'                   : essffw.FrameworkReg(0x40d , 0x0     , 'R'  , 0xFFFFFFFF),
   'BUILD_GIT_SHA1'                  : essffw.FrameworkReg(0x40e , 0x0     , 'R'  , 0xFFFFFFFF),
   'CTRL_RFQ_TRANSPARENCY'           : essffw.FrameworkReg(0x40f , 0x0     , 'RWY', 0x0000FFFF),
   'CTRL_DIFF_WARN_RESET'            : essffw.FrameworkReg(0x410 , 0x0     , 'W'  , 0x00000001),
   'CTRL_MIN_TRIG_PERIOD'            : essffw.FrameworkReg(0x411 , 0x0     , 'RWY', 0x0000FFFF),

   'CTRL_MAX_BEAMPULSE_WIDTH'        : essffw.FrameworkReg(0x412 , 0x0     , 'RWY', 0x00001FFF), 
   'CTRL_ENABLE_CALIBRATION_PULSE'   : essffw.FrameworkReg(0x413 , 0x0     , 'RWY', 0x0000001F),
   'CTRL_BEAM_ABOVE_THRESHOLD'       : essffw.FrameworkReg(0x414 , 0x0     , 'RWY', 0x0000FFFF),
   #'STATUS'                          : essffw.FrameworkReg(0x411 , 0x0     , 'RWY', 0x0000FFFF), 
   'MEAS_CLK_FREQ_KHZ'               : essffw.FrameworkReg(0x416 , 0x0     , 'R'  , 0x000FFFFF),
   'MEAS_TRIG_PERIOD'                : essffw.FrameworkReg(0x417 , 0x0     , 'R'  , 0xFFFFFFFF),
   'MEAS_TRIG_WID'                   : essffw.FrameworkReg(0x418 , 0x0     , 'R'  , 0x000FFFFF),
   'MEAS_TIME_FLATTOP'               : essffw.FrameworkReg(0x419 , 0x0     , 'R'  , 0x000FFFFF),

   'LUT_ADDR_REG'                    : essffw.FrameworkReg(0x41f , 0x0     , 'W'  , 0xFFFFFFFF),
   'LUT_DATA_REG'                    : essffw.FrameworkReg(0x420 , 0x0     , 'W'  , 0xFFFFFFFF),
   'BEAM_MODE_SW_SEL'                : essffw.FrameworkReg(0x421 , 0x0     , 'W'  , 0xFFFFFFFF),  
   'BEAM_TRIG'                       : essffw.FrameworkReg(0x422 , 0x0     , 'W'  , 0xFFFFFFFF),     
   'BEAM_TRIG_SRC'                   : essffw.FrameworkReg(0x423 , 0x0     , 'RWY', 0xFFFFFFFF),

   'ACQ_CH_MEM_OFFSET_ADDR'          : essffw.FrameworkReg(0x424 , 0x0     , 'RWY', 0xFFFFFFFF),
   'ACQ_NUM_SAMPLES'                 : essffw.FrameworkReg(0x425 , 0x0     , 'RWY', 0xFFFFFFFF),
   'ACQ_CH_NO'                       : essffw.FrameworkReg(0x426 , 0x0     , 'RWY', 0xFFFFFFFF),  

   'BEAM_EXIST_REG'                  : essffw.FrameworkReg(0x42d , 0x0     , 'R'  , 0x0000FFFF),
   'BEAM_MODE_REG'                   : essffw.FrameworkReg(0x42e , 0x0     , 'RWY', 0x0000000F)

}

adc_registers = {
  'ADC_TRIG_FINE_DELAY'            : essffw.FrameworkReg(0x00 , 0x0 , 'RWY' , 0x7FF),
  'ADC_SCALE'                      : essffw.FrameworkReg(0x01 , 0x0 , 'RWY' , 0xFFFF),
  'ADC_UPPER_THRESHOLD'            : essffw.FrameworkReg(0x02 , 0x0 , 'R'   , 0xFFFF),
  'ADC_LOWER_THRESHOLD'            : essffw.FrameworkReg(0x03 , 0x0 , 'R'   , 0xFFFF),
  'ADC_ERRANT_THRESHOLD'           : essffw.FrameworkReg(0x04 , 0x0 , 'RWY' , 0xFFFF),
  'ADC_OFFSET'                     : essffw.FrameworkReg(0x05 , 0x0 , 'RWY' , 0xFFFF), 
  'ADC_DROOPRATE'                  : essffw.FrameworkReg(0x06 , 0x0 , 'RWY' , 0xFFFF),
  'ADC_ENABLE_PARAM_CHOPPER_RFQ'   : essffw.FrameworkReg(0x07 , 0x0 , 'RWY' , 0x33),
  'ADC_MPS_ALARM_DISABLE'          : essffw.FrameworkReg(0x08 , 0x0 , 'RWY' , 0xFFFF),
  'MEAS_ADC_OFFS_ERR_SHORT'        : essffw.FrameworkReg(0x10 , 0x0 , 'R'   , 0xFFFFFFFF),
  'MEAS_ADC_OFFS_ERR_INTEG'        : essffw.FrameworkReg(0x11 , 0x0 , 'R'   , 0xFFFFFFFF),
  'MEAS_TIME_TRIG_TO_PULSE'        : essffw.FrameworkReg(0x12 , 0x0 , 'R'   , 0xFFFFFFFF),
  'MEAS_DROOP_ERR'                 : essffw.FrameworkReg(0x13 , 0x0 , 'R'   , 0xFFFFFFFF),
  'MEAS_BEAMPULSE_WIDTH'           : essffw.FrameworkReg(0x14 , 0x0 , 'R'   , 0xFFFFFFFF),
  'MEAS_CHARGE'                    : essffw.FrameworkReg(0x15 , 0x0 , 'R'   , 0xFFFFFFFF),
  'MEAS_CHARGE_FLATTOP'            : essffw.FrameworkReg(0x16 , 0x0 , 'R'   , 0xFFFFFFFF),
  'ADC_MPS_ALARM_HOLD'             : essffw.FrameworkReg(0x19 , 0x0 , 'R'   , 0xFFFFFFFF),
  'ADC_MPS_ALARM_FIRST'            : essffw.FrameworkReg(0x1A , 0x0 , 'R'   , 0xFFFFFFFF),
  'ADC_MAX_PULSE_LENGTH'           : essffw.FrameworkReg(0x1B , 0x0 , 'R'   , 0xFFFFFFFF),
  'LOWER_INTL_START_TIME'          : essffw.FrameworkReg(0x1D , 0x0 , 'RWY' , 0xFFFF),
  'LOWER_INTL_END_TIME'            : essffw.FrameworkReg(0x1E , 0x0 , 'RWY' , 0xFFFF),
  'ERRBEAM_INTL_START_TIME'        : essffw.FrameworkReg(0x1F , 0x0 , 'RWY' , 0xFFFF),
  'ERRBEAM_INTL_END_TIME'          : essffw.FrameworkReg(0x20 , 0x0 , 'RWY' , 0xFFFF),
  'CAL_SAMPLE1'                    : essffw.FrameworkReg(0x21 , 0x0 , 'R'   , 0xFFFFFFFF),
  'CAL_SAMPLE2'                    : essffw.FrameworkReg(0x22 , 0x0 , 'R'   , 0xFFFFFFFF),
  'CAL_SAMPLE3'                    : essffw.FrameworkReg(0x23 , 0x0 , 'R'   , 0xFFFFFFFF),
  'CAL_SAMPLE4'                    : essffw.FrameworkReg(0x24 , 0x0 , 'R'   , 0xFFFFFFFF),
  'BEAM_SIM_WD'                    : essffw.FrameworkReg(0x25 , 0x0 , 'WY' , 0xFFFFFFFF),
  'BEAM_SIM_DLY'                   : essffw.FrameworkReg(0x26 , 0x0 , 'WY' , 0xFFFFFFFF),
  'BEAM_SIM_AMP'                   : essffw.FrameworkReg(0x27 , 0x0 , 'WY' , 0xFFFF)

}

diff_registers = {
  'DIFF_SELECT_CHAN_INPUT_A'       : essffw.FrameworkReg(0x0  , 0x0 , 'RWY' , 0xF  ),
  'DIFF_SELECT_CHAN_INPUT_B'       : essffw.FrameworkReg(0x01 , 0x0 , 'RWY' , 0xF  ),
  'DIFF_DELAY'                     : essffw.FrameworkReg(0x02 , 0x0 , 'RWY' , 0x7FF),           
  'DIFF_RELAX_AT_SLOPE'            : essffw.FrameworkReg(0x03 , 0x0 , 'RWY' , 0xF  ),
  'DIFF_FAST_THRESH'               : essffw.FrameworkReg(0x04 , 0x0 , 'RWY' , 0x7FF),
  'DIFF_MEDIUM_THRESH'             : essffw.FrameworkReg(0x05 , 0x0 , 'RWY' , 0x7FFFFFFF),
  'DIFF_SLOW_THRESH'               : essffw.FrameworkReg(0x08  , 0x0 , 'RWY' , 0x7FFFFFFF),
  'DIFF_MPS_ALARM_DISABLE'         : essffw.FrameworkReg(0x09  , 0x0 , 'RWY' , 0xFFFF),
  'DIFF_FAST_MIN'                  : essffw.FrameworkReg(0x0A  , 0x0 , 'R'   , 0xFFFF),
  'DIFF_FAST_MAX'                  : essffw.FrameworkReg(0x0B  , 0x0 , 'R'   , 0xFFFF),
  'DIFF_MEDIUM_MIN'                : essffw.FrameworkReg(0x0C  , 0x0 , 'R'   , 0xFFFFFFFF),
  'DIFF_MEDIUM_MAX'                : essffw.FrameworkReg(0x0D  , 0x0 , 'R'   , 0xFFFFFFFF),
  'DIFF_SLOW_MIN'                  : essffw.FrameworkReg(0x0E  , 0x0 , 'R'   , 0xFFFFFFFF),
  'DIFF_SLOW_MAX'                  : essffw.FrameworkReg(0x0F  , 0x0 , 'R'   , 0xFFFFFFFF),
  'DIFF_MPS_ALARM_HOLD'            : essffw.FrameworkReg(0x010 , 0x0 , 'R'   , 0xFFFFFFFF),
  'DIFF_MPS_ALARM_FIRST'           : essffw.FrameworkReg(0x011 , 0x0 , 'R'   , 0xFFFFFFFF),
  'DIFF_INTL_START_TIME'           : essffw.FrameworkReg(0x012 , 0x0 , 'RWY' , 0xFFFF),
  'DIFF_INTL_END_TIME'             : essffw.FrameworkReg(0x013 , 0x0 , 'RWY' , 0xFFFF),
  'DIFF_WS_EMU_RFQ_EXIS'           : essffw.FrameworkReg(0x014 , 0x0 , 'R'   , 0x7)

}

fiber_registers = {
  'FIBER_OUT_DATA_SELECT'	   : essffw.FrameworkReg(0x0  , 0x0 , 'RWY' , 0xF  ),
  'FIBER_STATUS'	           : essffw.FrameworkReg(0x01 , 0x0 , 'R'   , 0xF  )
}

probe_registers = {
 'PROBE_SOURCE_SELECT'             : essffw.FrameworkReg(0x01 , 0x0 , 'WY' , 0x3F )
}
#-------------------------------------------------------------------------------
# Test DMA access to the board
#-------------------------------------------------------------------------------
def test_dma(device):

    print('------------------')
    print('DMA test:')
    print('------------------')

    errors  = essffw.check_memory_block(device, mem_size, 0x10000, 0x0000)
    errors += essffw.check_memory_block(device, mem_size, 0x10000, 0xFFFF)

    if errors == 0:
        test_passed = 1
    else:
        test_passed = 0

    return test_passed

#-------------------------------------------------------------------------------
# Test ADC clock frequency
#-------------------------------------------------------------------------------
def test_adc_clock(device):
    expected_freq = 125.0          # MHz
    accept_dev_factor = 0.02       # Deviation factor from expected value for still accepting
    measure_time = 0.1             # Measurement becomes too inaccurate below 0.05 (50 ms) measurement time
    counter_red_factor = 10        # given by the counter implementation in the FPGA
    sw_correction_cycles = 80000   # This value has been tried on one system, i.e. may be not very accurate

    print('')
    print('Test ADC clock frequency:')
    print( '-------------------------')

    # Measure number of cycles
    start_value = essffw.read_register(device, test_cstmlog_registers['CL_ADC_CLK_CNTR'].offset)
    time.sleep(measure_time)  # 100 ms
    end_value   = essffw.read_register(device, test_cstmlog_registers['CL_ADC_CLK_CNTR'].offset)

    if end_value > start_value:
        clk_cycles = end_value - start_value
    else:
        clk_cycles = end_value + ((2**32-1)-start_value)

    clk_cycles -= sw_correction_cycles
    clk_freq_mhz = counter_red_factor*(1.0/measure_time)*clk_cycles/10.0**6

    if abs(clk_freq_mhz-expected_freq) < (accept_dev_factor*expected_freq):
        status = "ok"
        test_passed = 1
    else:
        status = '\033[91m' + 'FAILED' + '\033[0m'
        test_passed = 0

    print('ADC clock frequency: ~ {0} MHz - {1}'.format(clk_freq_mhz, status))

    return test_passed

#-------------------------------------------------------------------------------
# Test ADC sampling
#-------------------------------------------------------------------------------
def test_adc(device):

    print('')
    print('ADC test:')
    print('------------------')

    adc_channels = 10
    adc_data_base_addr = 0
    sample_nr = 128 #*256/16
    sample_size = 2
    adc_buffer_size = sample_nr * sample_size
    adc_buffer_blocks = int(math.ceil(float(adc_buffer_size) / mem_block_size))
    print('adc_buffer_blocks = {0}'.format(adc_buffer_blocks))

    # Initialize ADC buffer memory with defined value to test if write from ADCs
    # takes place.
    init_value = 9999
    essffw.write_memory(device, 0x0000, 0x100000, init_value)

    # Initialize ADC memory regions with the channel number
  #  for channel_nr in range(1, adc_channels+1):
  #      area_offset = (channel_nr-1)*(adc_buffer_blocks*mem_block_size/2) + adc_data_base_addr
  #      essffw.write_memory(device, area_offset, adc_buffer_size/2, channel_nr)

    # Set up registers for ADC data acqusition
    channel_block_addr = {}
    essffw.write_register(device, essffw.registers['ADC_SAMPLE_LEN'].offset, adc_buffer_blocks-1)
    for channel_nr in range(1, adc_channels+1):
        reg_name = 'ADC_CH' + str(channel_nr) + '_MEM'
        adc_block_addr = (channel_nr-1)*(adc_buffer_blocks) + (adc_data_base_addr/mem_block_size)
        print('ADC_CH_MEM({0})= {1} '.format(channel_nr,adc_block_addr))
        essffw.write_register(device, essffw.registers[reg_name].offset, adc_block_addr)
        channel_block_addr[channel_nr] = adc_block_addr

    # Check if registers are correct for following DAQ
  
#    essffw.write_register(device, essffw.registers['ADC_CH1_MEM'].offset, 0x0)
#    essffw.write_register(device, essffw.registers['ADC_CH2_MEM'].offset, 0x100)
#    essffw.write_register(device, essffw.registers['ADC_CH3_MEM'].offset, 0x200)
#    essffw.write_register(device, essffw.registers['ADC_CH4_MEM'].offset, 0x300)
#    essffw.write_register(device, essffw.registers['ADC_CH5_MEM'].offset, 0x400)
#    essffw.write_register(device, essffw.registers['ADC_CH6_MEM'].offset, 0x500)
#    essffw.write_register(device, essffw.registers['ADC_CH7_MEM'].offset, 0x600)
#    essffw.write_register(device, essffw.registers['ADC_CH8_MEM'].offset, 0x700)
#    essffw.write_register(device, essffw.registers['ADC_CH9_MEM'].offset, 0x800)
#    essffw.write_register(device, essffw.registers['ADC_CH10_MEM'].offset, 0x900)

    # Trigger the data acquisition and wait 10 ms for values to be recorded
    essffw.write_register(device, essffw.registers['ADC_ACQ_CTRL'].offset, 0x00000001)
    essffw.write_register(device, essffw.registers['ADC_ACQ_CTRL'].offset, 0x00000000)
    time.sleep(0.01) # 10 ms
#    essffw.write_register(device, essffw.registers['ADC_ACQ_CTRL'].offset, 0x00000002)
#    essffw.write_register(device, essffw.registers['ADC_ACQ_CTRL'].offset, 0x00000000)
#    time.sleep(1)


    # Test if sampling finshes
    sampling_active = essffw.read_register(device, essffw.registers['ADC_ACQ_CTRL'].offset)
    if (sampling_active & 0x00000001) != 0:
        print('ERROR: Sampling does not stop.')
        exit()

    # Read back values from the ADC buffers
    test_passed = 1
    for channel_nr in range(1, adc_channels+1):
        reg_name = 'ADC_CH' + str(channel_nr) + '_MEM'
        recorded_blocks = essffw.read_register(device, essffw.registers[reg_name].offset)
        recorded_blocks -= channel_block_addr[channel_nr]
        recorded_samples = recorded_blocks * mem_block_size / sample_size
        if sample_nr == recorded_samples:
            test_status = 'PASSED'
        else:
            test_status = '\033[91m' + 'FAILED' + '\033[0m'
            test_passed = 0
        print('ADC channel {0}: {1} samples written to 0x{2:x} -- test {3}'.format(channel_nr, recorded_samples, channel_block_addr[channel_nr]*mem_block_size, test_status))

        changed_values = 0
        area_offset = (channel_nr-1)*(adc_buffer_blocks*mem_block_size/2) + adc_data_base_addr
        adc_data = essffw.read_memory(device, area_offset, sample_nr)
        for adc_value in adc_data.splitlines():
            if int(adc_value) != init_value:
                changed_values += 1
        if changed_values < sample_nr:
            test_result = '\033[91m' + 'FAILED' + '\033[0m'
            test_passed = 0
        else:
            test_result = 'PASSED'

        print('ADC channel {0}: buffer changed at {1} of {2} locations ({3}%)-- test {4}'.format(channel_nr, changed_values, (adc_buffer_size/sample_size), 100*changed_values/(adc_buffer_size/sample_size), test_result))

    return test_passed

#-------------------------------------------------------------------------------
# Test custom logic memory write interface
#-------------------------------------------------------------------------------
def test_custom_logic_write_if(device):

    print('')
    print('Custom Logic: memory write interface test:')
    print('-----------------------------------------')

    # Initialize memory with zeroes.
    init_value = 0x0000
    custom_write_value = 0x5555
    nr_test_values = 128

    essffw.write_memory(device, 0x00000000, 0x100000, init_value)

    essffw.write_register(device, test_cstmlog_registers['CL_MEM_WR_BASE_ADDR'].offset, 0x00000020)
    essffw.write_register(device, test_cstmlog_registers['CL_MEM_IF_CTRL'].offset, 0x00000001)
    essffw.write_register(device, test_cstmlog_registers['CL_MEM_IF_CTRL'].offset, 0x00000002)
    essffw.write_register(device, test_cstmlog_registers['CL_MEM_IF_CTRL'].offset, 0x00000000)

    mem_data = essffw.read_memory(device, 0x00000000, nr_test_values)
    i = 0
    correct_values = 0
    for mem_value in mem_data.splitlines():
        mem_value = int(mem_value)
        if i < 16:
            if mem_value == init_value:
                correct_values = correct_values + 1
            else:
                print('Failed: {0} != {1}'.format(mem_value, init_value))
        else:
            if mem_value == custom_write_value:
                correct_values = correct_values + 1
            else:
                print( 'Failed: {0} != {1}'.format(mem_value, custom_write_value))
        i = i + 1

    if correct_values == nr_test_values:
        print('Custom logic wrote successfully to memory.')
        test_passed = 1
    else:
        print('Custom logic \033[91m' + 'FAILED' + '\033[0m in writing to the memory in {0} cases.'.format(nr_test_values-correct_values))
        test_passed = 0

    return test_passed

#-------------------------------------------------------------------------------
# Test custom logic memory read interface
#-------------------------------------------------------------------------------
def test_custom_logic_read_if(device):

    print('')
    print('Custom Logic: memory read interface test:')
    print('-----------------------------------------')

    # Initialize memory with alternating bits.
    test_passed = 1
    init_value = 0x5555
    essffw.write_memory(device, 0x00000000, 0x100000, init_value)
    slice_insert_value = 0xABCD
    essffw.write_memory(device, 0x00000020, 32, slice_insert_value)

    essffw.write_register(device, test_cstmlog_registers['CL_MEM_LD_BASE_ADDR'].offset, 0x00000000)
    essffw.write_register(device, test_cstmlog_registers['CL_MEM_IF_CTRL'].offset, 0x00000008)
    essffw.write_register(device, test_cstmlog_registers['CL_MEM_IF_CTRL'].offset, 0x00000000)

    essffw.write_register(device, test_cstmlog_registers['CL_SP_ADDR'].offset, 0x00000000)
    sp_value = essffw.read_register(device, test_cstmlog_registers['CL_SP_ADDR'].offset)
    if sp_value == ((init_value << 16) | init_value):
        status_F = 'ok'
    else:
        status_F = '\033[91m' + 'fail' + '\033[0m'
        test_passed = 0
        print('Value 0x{0:x} read from the first position in the SP table: {1}'.format(sp_value, status_F))

    essffw.write_register(device, test_cstmlog_registers['CL_SP_ADDR'].offset, 0x00000002)
    sp_value = essffw.read_register(device, test_cstmlog_registers['CL_SP_ADDR'].offset)
    if sp_value == ((slice_insert_value << 16) | slice_insert_value):
        status_F = 'ok'
    else:
        status_F = '\033[91m' + 'fail' + '\033[0m'
        test_passed = 0
        print('Value 0x{0:x} read from the third position in the SP table: {1}'.format(sp_value, status_F))

    essffw.write_register(device, test_cstmlog_registers['CL_MEM_LD_BASE_ADDR'].offset, 0x00000040)
    essffw.write_register(device, test_cstmlog_registers['CL_MEM_IF_CTRL'].offset, 0x0000000C)
    essffw.write_register(device, test_cstmlog_registers['CL_MEM_IF_CTRL'].offset, 0x00000000)

    essffw.write_register(device, test_cstmlog_registers['CL_FF_ADDR'].offset, 0x00000000)
    ff_value = essffw.read_register(device, test_cstmlog_registers['CL_FF_ADDR'].offset)
    if ff_value == ((slice_insert_value << 16) | slice_insert_value):
        status_F = 'ok'
    else:
        status_F = '\033[91m' + 'fail' + '\033[0m'
        test_passed = 0
        print('Value 0x{0:x} read from the first position in the FF table: {1}'.format(ff_value, status_F))

    essffw.write_register(device, test_cstmlog_registers['CL_FF_ADDR'].offset, 0x00000002)
    ff_value = essffw.read_register(device, test_cstmlog_registers['CL_FF_ADDR'].offset)
    if ff_value == ((init_value << 16) | init_value):
        status_F = 'ok'
    else:
        status_F = '\033[91m' + 'fail' + '\033[0m'
        test_passed = 0
        print('Value 0x{0:x} read from the third position in the FF table: {1}'.format(ff_value, status_F))

    return test_passed

#-------------------------------------------------------------------------------
#-- Test Interlock masking look up table
#-------------------------------------------------------------------------------
def test_interlock_LUT(device):
    # filling up the look up table with a counter
    print('****************************************************')
    print(' Test of Interlock Look-up table')
    print('****************************************************')
    essffw.write_register(device, bcm_common_registers['LUT_ADDR_REG'].offset, 0x400)
    for i in range(16):
      essffw.write_register(device, bcm_common_registers['LUT_DATA_REG'].offset, i)

    # set beam mode and check the contents of look up table according to mead mode value
    # beam mode value has been connected to the LUT mem address
    status_F = '\033[92m' +' PASS '+ '\033[0m'
    for i in range(16):
      essffw.write_register(device, bcm_common_registers['BEAM_MODE_REG'].offset, i)
      ff_value = essffw.read_register(device, bcm_common_registers['BEAM_EXIST_REG'].offset)
      if (ff_value != i ):
        status_F = '\033[91m' + ' FAIL' + '\033[0m'
    print('The interlock LUT test : {0}'.format(status_F))

#-------------------------------------------------------------------------------
#-- Test ACCTs parameters look up table
#-------------------------------------------------------------------------------
def test_acct_param_LUT(device):
    # filling up the look up table with a counter
    print('****************************************************')
    print( ' Test of ACCTs parameters Look-up table')
    print( '****************************************************')
    
    max_pulse_length_offset = 0x0
    lower_thr_offset = 0x010
    upper_thr_offset = 0x020
    ch_offset = 0x40
    for j in range(10):
      essffw.write_register(device, bcm_common_registers['LUT_ADDR_REG'].offset, max_pulse_length_offset+j*ch_offset)
      for i in range(16):
        essffw.write_register(device, bcm_common_registers['LUT_DATA_REG'].offset, i+j*16)

    for j in range(10):
      essffw.write_register(device, bcm_common_registers['LUT_ADDR_REG'].offset, lower_thr_offset+j*ch_offset)
      for i in range(16):
        essffw.write_register(device, bcm_common_registers['LUT_DATA_REG'].offset, i+j*256)

    for j in range(10):
      essffw.write_register(device, bcm_common_registers['LUT_ADDR_REG'].offset, upper_thr_offset+j*ch_offset)
      for i in range(16):
        essffw.write_register(device, bcm_common_registers['LUT_DATA_REG'].offset, i+j*4096)


    # set beam mode and check the contents of look up table according to mead mode value
    # beam mode value has been connected to the LUT mem address
    status_F = '\033[92m' +' PASS '+ '\033[0m'
    for ch in range(10):
      for i in range(16):
        essffw.write_register(device, bcm_common_registers['BEAM_MODE_REG'].offset, i)
        ff_value = essffw.read_register(device, adc_registers['ADC_UPPER_THRESHOLD'].offset+0x500+0x100*ch)
        if (ff_value != (i+ch*0x1000) ):
          status_F = '\033[91m' + ' FAIL' + '\033[0m'
          print('ADC_UPPER_THRESHOLD_CH{0} : {1}'.format(ch,status_F))
        #---------------------------------------------------------------------------------------------------
        ff_value = essffw.read_register(device, adc_registers['ADC_LOWER_THRESHOLD'].offset+0x500+0x100*ch)
        if (ff_value != (i+ch*0x100) ):
          status_F = '\033[91m' + ' FAIL' + '\033[0m'
          print('ADC_LOWER_THRESHOLD_CH{0} : {1}'.format(ch,status_F))
        #---------------------------------------------------------------------------------------------------
        ff_value = essffw.read_register(device, adc_registers['ADC_MAX_PULSE_LENGTH'].offset+0x500+0x100*ch)
        if (ff_value != (i+ch*0x10) ):
          status_F = '\033[91m' + ' FAIL' + '\033[91m'
          print('ADC_MAX_PULSE_LENGTH_CH{0} : {1}'.format(ch,status_F))

    print('The ACCT param LUT test : {0}'.format(status_F))


#-------------------------------------------------------------------------------
#-- BCM functionality Test
#-------------------------------------------------------------------------------
def test_bcm(device):
    # filling up the look up table with a counter
    print('****************************************************')
    print(' BCM Functionality Test')
    print('****************************************************')
    #essffw.write_register(device, 0x40, 0xf00)
    print(' Setting External Clock for ADCs and the processing clock.')
    ff_value = essffw.read_register(device, bcm_common_registers['MEAS_CLK_FREQ_KHZ'].offset)
    proc_period = 1.0/(ff_value);
    print(' Proc. clock freq: {0} KHz'.format(ff_value))
    print(' Setting the trigger source to the internally generated inside of FPGA.')
    essffw.write_register(device, bcm_common_registers['BEAM_TRIG_SRC'].offset, 0x01) # FPGA internal trigger

    #---------------------------------------------------------------------------

    for i in range(100):
      ff_value = essffw.read_register(device, bcm_common_registers['MEAS_TRIG_PERIOD'].offset)*proc_period
      if(ff_value > 73 or ff_value < 70):
        print('Trig Period: {0} ms '.format(ff_value))
    print(' Trig Period: {0} ms'.format(ff_value))

    for i in range(100):
      ff_value = essffw.read_register(device, bcm_common_registers['MEAS_TRIG_WID'].offset)*proc_period
      if(ff_value > 2.9 or ff_value < 2.7):
        print(bcolors.FAIL +' Trig Width: {0} ms'.format(ff_value)+ bcolors.ENDC)
    print(' Trig Width: {0} ms'.format(ff_value))
    for i in range(100):
      ff_value = essffw.read_register(device, bcm_common_registers['MEAS_TIME_FLATTOP'].offset)*proc_period
      if(ff_value > 2.9 or ff_value < 2.7):
        print(bcolors.FAIL +' Time flat top: {0} ms'.format(ff_value)+ bcolors.ENDC)
    print(' Time flat top: {0} ms'.format(ff_value))

    #------------------------------------------------------------------------------
    #-- Ch recorder setting
    #------------------------------------------------------------------------------

    for ch in range(16):
      essffw.write_register(device, bcm_common_registers['ACQ_CH_NO'].offset,  ch) 
      essffw.write_register(device, bcm_common_registers['ACQ_NUM_SAMPLES'].offset, 128*64) 
      essffw.write_register(device, bcm_common_registers['ACQ_CH_MEM_OFFSET_ADDR'].offset, 0x10000*ch) 

    
    #------------------------------------------------------------------------------
    #-- Pulse simulator setting
    essffw.write_register(device, adc_registers['BEAM_SIM_WD'].offset+0x500,  100) 
    essffw.write_register(device, adc_registers['BEAM_SIM_DLY'].offset+0x500, 5000) 
    essffw.write_register(device, adc_registers['BEAM_SIM_AMP'].offset+0x500, 20000) 

    essffw.write_register(device, adc_registers['BEAM_SIM_WD'].offset+0xe00,  100) 
    essffw.write_register(device, adc_registers['BEAM_SIM_DLY'].offset+0xe00, 5500) 
    essffw.write_register(device, adc_registers['BEAM_SIM_AMP'].offset+0xe00, 20000) 

#-------------------------------------------------------------------------------
# Plot recoreded data
#-------------------------------------------------------------------------------

def plot_rec_data(device):
   # get data from device
   fig, axs = plt.subplots(8, 2)   

   d = open(device, "rb")
   for i in range(16):
     d.seek(0x10000*i)
     data = d.read(0x8000)
     si = 0x0
     pd = []
     for j in range(0, 1000):
       si = si + 4
       s = struct.unpack('<h', data[si:si+2])
       pd.append(s[0])
     #axs[i,0].psd(pd, NFFT=4096*8 ,Fs=1 , Fc=0, noverlap=0, pad_to=None,sides='default', scale_by_freq=None, return_line=None)
     if(i<8):
       axs[i,0].plot(pd, color='green', linewidth=1)
     else:
       axs[i-8,1].plot(pd, color='red', linewidth=1)
   d.close()
   plt.show()
	 
#-------------------------------------------------------------------------------
# Check BCM core features
#-------------------------------------------------------------------------------
def bcm_core_test(device):
    for i in range(10):
      essffw.write_register(device, adc_registers['ADC_ENABLE_PARAM_CHOPPER_RFQ'].offset+0x500+0x100*i,  0x0) 
      essffw.write_register(device, adc_registers['ADC_SCALE'].offset+0x500+0x100*i,  0xFFFF)

#-------------------------------------------------------------------------------
# Test procedure
#-------------------------------------------------------------------------------

print('')
print( '****************************************************')
print( ' Test of ESS FPGA Framework on SIS8300-KU')
print( '****************************************************')
print( '')

sis8300ku_devices = essffw.get_sis8300ku()
if len(sis8300ku_devices) < 1:
    exit()
device = sis8300ku_devices[0]

all_registers = dict(essffw.registers)
#all_registers.update(test_cstmlog_registers)

all_registers.update(bcm_common_registers)

#--------------------------------------------------------------
#---- Build adc and diff ch regiters
adc_ch_registers   = []
diff_ch_registers  = []
fiber_ch_registers = []
probe_ch_registers = []

for i in range(10):
  temp = adc_registers.copy()
  for reg_name, reg_info in temp.items():
      value  = temp[reg_name].offset + (1024+256*(i+1))
      temp[reg_name] = temp[reg_name]._replace(offset = value)
      temp[reg_name+'_ch'+str(i)] = temp.pop(reg_name) 
  adc_ch_registers.append(temp)
  diff_temp = diff_registers.copy()
  for reg_name, reg_info in diff_temp.items():
      value = diff_temp[reg_name].offset + (1088+256*(i+1))
      diff_temp[reg_name] = diff_temp[reg_name]._replace(offset = value)
      diff_temp[reg_name+'_ch'+str(i)] = diff_temp.pop(reg_name)
  diff_ch_registers.append(diff_temp)
  all_registers.update(adc_ch_registers[i])
  all_registers.update(diff_ch_registers[i])

#------------------------------------------------------------
#--- Build fiber ch registers
for i in range(2):
   fiber_temp = fiber_registers.copy()
   for reg_name, reg_info in fiber_temp.items():
      value  = fiber_temp[reg_name].offset + (1152+256*(i+1))
      fiber_temp[reg_name] = fiber_temp[reg_name]._replace(offset = value)
      fiber_temp[reg_name+'_ch'+str(i)] = fiber_temp.pop(reg_name)
   fiber_ch_registers.append(fiber_temp)
   all_registers.update(fiber_ch_registers[i])

#-----------------------------------------------------------
#--- Bulid probe ch registers
for i in range(4):
   probe_temp = probe_registers.copy()
   for reg_name, reg_info in probe_temp.items():
      value  = probe_temp[reg_name].offset + (1216+256*(i+1))
      probe_temp[reg_name] = probe_temp[reg_name]._replace(offset = value)
      probe_temp[reg_name+'_ch'+str(i)] = probe_temp.pop(reg_name)
   probe_ch_registers.append(probe_temp)
   all_registers.update(probe_ch_registers[i])

       


passed_tests = 0
run_tests = 0

#passed_tests += essffw.test_registers_after_reset(device, all_registers)
run_tests += 1


#passed_tests += essffw.test_register_write(device, all_registers)
run_tests += 1

#passed_tests += test_dma(device)
run_tests += 1

#passed_tests += test_adc_clock(device)
run_tests += 1

#passed_tests += test_adc(device)
run_tests += 1

#passed_tests += test_custom_logic_write_if(device)
run_tests += 1

#passed_tests += test_custom_logic_read_if(device)
run_tests += 1

#test_interlock_LUT(device)

#test_acct_param_LUT(device)

test_bcm(device)
#plot_rec_data(device)
#bcm_core_test(device)
print( '')
print( 'Summary: {0} of {1} tests passed'.format(passed_tests, run_tests))
print('')

