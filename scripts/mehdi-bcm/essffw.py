#-------------------------------------------------------------------------------
#  Project     : ESS FPGA Framework
#-------------------------------------------------------------------------------
#  File        : essffw.py
#  Authors     : Christian Amstutz
#  Created     : 2018-01-18
#  Last update : 2018-02-22
#  Platform    : SIS8300-KU
#  Standard    :
#-------------------------------------------------------------------------------
#  Description : Functions to access FPGA boards running with the ESS FPGA
#                Framework.
#  Problems    :
#-------------------------------------------------------------------------------
#  Revisions  :
#
#  0.01 : 2018-02-22  Christian Amstutz
#         Created by refactoring board_test.py
#-------------------------------------------------------------------------------
#  Copyright (c) 2018 European Spallation Source ERIC
#-------------------------------------------------------------------------------

import subprocess
import time

from collections import namedtuple
from collections import OrderedDict

# Modes:
#  R : register can be read
#  W : register can be written
#  X : writing triggers some actions with side effects, e.g. start DMA
#  Y : value read back is the same as previously written
FrameworkReg = namedtuple('FrameworkReg', ['offset', 'reset_value', 'mode', 'mask'])

registers = {
    'MODULE_ID'            : FrameworkReg(0x000, 0x83032802, 'R'  , 0xFFFFFFFF),
    'SERIAL_NR'            : FrameworkReg(0x001, 0x00000005, 'R'  , 0xFFFFFFFF),
    'FIRMWARE_OPTIONS'     : FrameworkReg(0x005, 0x00000000, 'R'  , 0xFFFFFFFF),
    'PCIE_STATUS'          : FrameworkReg(0x007, None,       'R'  , 0xFFFFFFFF),
    'ADC_ACQ_CTRL'         : FrameworkReg(0x010, 0x00000080, 'RWX', 0xFFFFFFFF),
    'RESET'                : FrameworkReg(0x0FF, 0x00000000, 'WX' , 0xFFFFFFFF),
    'ADC_CH1_MEM'          : FrameworkReg(0x120, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH2_MEM'          : FrameworkReg(0x121, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH3_MEM'          : FrameworkReg(0x122, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH4_MEM'          : FrameworkReg(0x123, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH5_MEM'          : FrameworkReg(0x124, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH6_MEM'          : FrameworkReg(0x125, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH7_MEM'          : FrameworkReg(0x126, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH8_MEM'          : FrameworkReg(0x127, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH9_MEM'          : FrameworkReg(0x128, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_CH10_MEM'         : FrameworkReg(0x129, 0x00000000, 'RW' , 0xFFFFFFFF),
    'ADC_SAMPLE_LEN'       : FrameworkReg(0x12A, 0x00000000, 'RW' , 0xFFFFFFFF),
    'DMA_READ_DST_ADR_LO'  : FrameworkReg(0x200, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_READ_DST_ADR_HI'  : FrameworkReg(0x201, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_READ_SRC_ADR_LO'  : FrameworkReg(0x202, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_READ_LEN'         : FrameworkReg(0x203, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_READ_CTRL'        : FrameworkReg(0x204, 0x00000000, 'RX' , 0xFFFFFFFF),
    'DMA_BYTE_SWAP_CTRL'   : FrameworkReg(0x205, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_WRITE_DST_ADR_LO' : FrameworkReg(0x210, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_WRITE_DST_ADR_HI' : FrameworkReg(0x211, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_WRITE_SRC_ADR_LO' : FrameworkReg(0x212, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_WRITE_LEN'        : FrameworkReg(0x213, 0x00000000, 'RWY', 0xFFFFFFFF),
    'DMA_WRITE_CTRL'       : FrameworkReg(0x214, 0x00000000, 'RX' , 0xFFFFFFFF),
    'IRQ_ENABLE'           : FrameworkReg(0x220, 0x00000000, 'R'  , 0xFFFFFFFF),
    'IRQ_STATUS'           : FrameworkReg(0x221, 0x00000000, 'R'  , 0xFFFFFFFF),
    'IRQ_CLEAR'            : FrameworkReg(0x222, 0x00000000, 'W'  , 0xFFFFFFFF)
}
#-------------------------------------------------------------------------------
def get_sis8300ku():
    sis8300_ku_devices = []
    devices = subprocess.check_output(["ls", "/dev"])
    devices = devices.splitlines()
    sis8300_devices = filter(lambda x: x.startswith('sis8300-'), devices)

    # Check if devices are KU versions
    for sis in sis8300_devices:
        module_id = read_register('/dev/' + sis, registers['MODULE_ID'].offset)
        if (module_id & 0xFFFF0000) == 0x83030000:
            device_file = '/dev/' + sis
            sis8300_ku_devices.append(device_file)

    if len(sis8300_ku_devices) > 0:
        print( '{0} SIS8300-KU devices found in the system: {1}'.format(len(sis8300_ku_devices), str(' '.join(sis8300_ku_devices))))
    else:
        print('ERROR: No SIS8300-KU devices found in the system.')
    return sis8300_ku_devices

#-------------------------------------------------------------------------------
def read_register(device, offset):
    result = subprocess.check_output(["sis8300drv_reg", device, hex(offset)])
    return int(result, 0)

#-------------------------------------------------------------------------------
def write_register(device, offset, value):
    result = subprocess.check_output(["sis8300drv_reg", device, hex(offset), '-w', hex(value)])

#-------------------------------------------------------------------------------
def check_register_write(device, offset, test_value):
    write_register(device, offset, test_value)
    result = read_register(device, offset)
    if result == test_value:
        return 1
    else:
        return 0

#-------------------------------------------------------------------------------
def wait_for_acq_done(device):
    result = subprocess.check_output(["sis8300drv_irq", device, '-i 0']) #-t 100 
    return result
#-------------------------------------------------------------------------------
def read_memory(device, offset, nr_samples):
    if (nr_samples % 32) != 0:
        print( 'ERROR: Problem with memory access size, must be a mulitple of 32: {0}'.format(nr_samples))
        exit()

    result = subprocess.check_output(["sis8300drv_mem", device, '-o', str(offset), '-n', str(nr_samples)])
    return result

#-------------------------------------------------------------------------------
def write_memory(device, offset, nr_samples, value):
    if (nr_samples % 32) != 0:
        print( 'ERROR: Problem with memory access size, must be a mulitple of 32: {0}'.format(nr_samples))
        exit()

    result = subprocess.check_output(["sis8300drv_mem", device, '-o', str(offset), '-n', str(nr_samples), '-w' ,str(value)])

#-------------------------------------------------------------------------------
def check_memory_block(device, mem_size, dma_block_size, value):
    print( 'writing 0x{0:x} to memory'.format(value))
    start = time.time()
    for offset in range(0, mem_size, dma_block_size):
        write_memory(device, offset, dma_block_size, value)
    end = time.time()
    print( 'writing done in {0} s'.format(end-start))

    print( 'read back data from memory')
    errors = 0
    start = time.time()
    counter = 0
    for offset in range(0, mem_size, dma_block_size):
        counter += 1
        result = (read_memory(device, offset, dma_block_size))
        for read_value in result.splitlines():
            if int(read_value) != value:
                errors += 1
    end = time.time()
    nr_bytes = dma_block_size * counter
    mbit_per_s = nr_bytes * 8 / (end-start) / 1000000
    print( 'read {0} Bytes done with {1} errors in {2} s --> {3} Mbit/s'.format(nr_bytes, errors, end-start, mbit_per_s))

    return errors

#-------------------------------------------------------------------------------
# Test for the correct register value after reset
#-------------------------------------------------------------------------------
def test_registers_after_reset(device, registers):
    print( '')
    print( 'Register name , address ,read values , expected value after reset , status :')
    print( '----------------------------------------------------------------------------')

    registers_by_offset = OrderedDict(sorted(registers.items(), key=lambda t: t[1].offset))

    test_passed = 1
    for reg_name, reg_info in registers_by_offset.items():
        write_register(device, reg_info.offset, 0)
        if 'R' in reg_info.mode:
            register_value = read_register(device, reg_info.offset)

            if reg_info.reset_value == None:
                status = ''
            elif register_value == reg_info.reset_value:
                status = 'ok'
            else:
                status = '\033[91m' + 'FAIL' + '\033[0m'
                test_passed = 0
                print( '   {0:<25} (0x{1:0>3x}): 0x{2:0>8x} : 0x{3:0>8x} - {4}'.format(reg_name,reg_info.offset, register_value, reg_info.reset_value , status))

    return test_passed

#-------------------------------------------------------------------------------
# Test if registers return the same value as was written to it, only applies to
# registers with mode 'RWY'.
#-------------------------------------------------------------------------------
def test_register_write(device, registers):

    print( '')
    print( 'Register write test:  write all 0x00000000, then write all 0xFFFFFFFF')
    print( '---------------------------------------------------------------------')

    registers_by_offset = OrderedDict(sorted(registers.items(), key=lambda t: t[1].offset))

    test_passed = 1
    for reg_name, reg_info in registers_by_offset.items():
        if ('R' in reg_info.mode) and ('W' in reg_info.mode) and ('Y' in reg_info.mode):

            # Initialize with 0xFFFFFFFF to check if 0s written correctly
            write_register(device, reg_info.offset, 0xFFFFFFFF & reg_info.mask)

            if check_register_write(device, reg_info.offset, 0x00000000):
                status_0 = 'ok'
            else:
                status_0 = '\033[91m' + 'FAIL' + '\033[0m'
                test_passed = 0

            if check_register_write(device, reg_info.offset, 0xFFFFFFFF & reg_info.mask):
                status_F = 'ok'
            else:
                status_F = '\033[91m' + 'FAIL' + '\033[0m'
                test_passed = 0

                register_line = '   {0:<25} (0x{1:0>3x}): {2:>4} - {3:>4}'.format(reg_name,reg_info.offset, status_0, status_F)
                print( register_line)
    if (test_passed == 1 ):
      status_F = '\033[92m' +' PASS '+ '\033[0m'
    else:
      status_F = '\033[91m' + ' FAIL' + '\033[0m'
    print( 'Register write test : {0}'.format(status_F))

    return test_passed
