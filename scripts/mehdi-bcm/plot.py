#-------------------------------------------------------------------------------
#  Project     : ESS FPGA Framework
#-------------------------------------------------------------------------------
#  File        : plot.py
#  Authors     : Mehdi Mohammadnezhad
#  Created     : 2017-10-05
#  Last update : 2018-07-19
#  Platform    : SIS8300-KU
#  Standard    :
#-------------------------------------------------------------------------------
#  Description : Test script to test functionality of the ESS FPGA Framework on
#                the SIS8300-KU board
#  Problems    :
#-------------------------------------------------------------------------------
#  Copyright (c) 2018 European Spallation Source ERIC
#-------------------------------------------------------------------------------
#  Revisions  :
#
#  0.01 : 2018-07-19  Christian Amstutz
#         Created
#-------------------------------------------------------------------------------

import time
import math
import struct
import matplotlib.pyplot as plt
import essffw
import essbcm
import matplotlib.animation as anim



#-------------------------------------------------------------------------------
# Plot recoreded data
#-------------------------------------------------------------------------------
def plot_update(device,xmax):
   fig = plt.figure()
   axs = plt.subplots(8, 2)   
   def plot_rec_data():
     d = open(device, "rb")
     for i in range(16):
       d.seek(0x10000*i)
       data = d.read(0x8000)
       si = 0x0
       pd = []
       for j in range(0, 1000):
         si = si + 4
         s = struct.unpack('<h', data[si:si+2])
         pd.append(s[0])
         #axs[i,0].psd(pd, NFFT=4096*8 ,Fs=1 , Fc=0, noverlap=0, pad_to=None,sides='default', scale_by_freq=None, return_line=None)
       
       if(i<8):
         axs[i,0].plot(pd, color='green', linewidth=1)
       else:
         axs[i-8,1].plot(pd, color='red', linewidth=1)
     d.close()
 
     
  
   #a = anim.FuncAnimation(fig, plot_rec_data, frames=xmax, repeat=True)
   print(fig)
   fig.show()
   fig.canvas.draw()
   plot_rec_data()
   
#-------------------------------------------------------------------------------
# Test procedure
#-------------------------------------------------------------------------------

print('')
print( '****************************************************')
print( ' Test of ESS FPGA Framework on SIS8300-KU')
print( '****************************************************')
print( '')
device = '/dev/sis8300-5'
essffw.wait_for_acq_done(device)
#plot_update(device,1)

#for i in range(10):
#  time.sleep(1)
#  plot_rec_data(device);



