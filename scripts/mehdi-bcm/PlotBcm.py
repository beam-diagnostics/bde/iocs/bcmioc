import sys
import os
import inspect
import time
import struct
#import essffw
#import essbcm
import numpy as np
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QMenu, QVBoxLayout, QSizePolicy,QComboBox, QMessageBox, QWidget, QPushButton, QLabel, QGridLayout
from PyQt5.QtGui     import QFont
from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

import_folder = "ess-bcm-struck-sis8300-ku/sw/python/bcm"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import bcm

import_folder = "ess-bcm-struck-sis8300-ku/lib/hw/ess_fpga_framework/sw/python"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import essffw



if is_pyqt5():
    from matplotlib.backends.backend_qt5agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
else:
    from matplotlib.backends.backend_qt4agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.device = essffw_devices[0] 
        self.first = 1;
        self.ch1 = 10;
        self.ch2 = 22;
        self.ch3 = 20;
        
        self.en_ch1_ax = 0

        self.log_file = open("log.txt","w+");
        self.cn = 0;        
        self.current_avg = 0.0

        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)

        layout = QGridLayout(self._main)
        layout.setSpacing(20)

        newfont = QFont("Times", 18, QFont.Bold) 
        samllfont = QFont("Times", 12, QFont.Bold) 

        self.x1_default = -1.0
        self.x2_default = 12.0*1
        self.y1_default = -20.0
        self.y2_default = 100.0 


        self.y1 = self.y1_default
        self.y2 = self.y2_default

        self.x1 = self.x1_default
        self.x2 = self.x2_default

        #layout = QtWidgets.QVBoxLayout(self._main)

        self.lb_Cur = QLabel("Beam Current :")
        self.lb_Cur.move(20, 100) 
        self.lb_Cur.setFont(newfont)
        layout.addWidget(self.lb_Cur,1, 7)

        self.lb_BW = QLabel("Beam Width :")
        self.lb_BW.setFont(newfont)
        layout.addWidget(self.lb_BW,1, 10)

        self.lb_TW = QLabel("Trig Width :")
        self.lb_TW.setFont(newfont)
        layout.addWidget(self.lb_TW,2, 7)


        self.lb_FTW = QLabel("Flat top width :")
        self.lb_FTW.setFont(newfont)
        layout.addWidget(self.lb_FTW,2, 10)

        Rst = QPushButton("Alarm Reset")
        layout.addWidget(Rst,1, 19)
        Rst.clicked.connect(self.onRst)

        self.Plotrun = 1; 
        self.plotBt = QPushButton("Stop")
        layout.addWidget(self.plotBt,2, 19)
        self.plotBt.clicked.connect(self.onplotBt)

        #static_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        #layout.addWidget(static_canvas)
        #self.addToolBar(NavigationToolbar(static_canvas, self))

        lb_ch1 = QLabel("Plot1:")
        lb_ch1.setFont(samllfont)
        lb_ch1.move(0, 100)
        layout.addWidget(lb_ch1,1, 1)
        #layout.addWidget(lb_ch1)
        combo = QComboBox(self)
        for i in range(26):
         combo.addItem(str(i))
        combo.move(80, 10)
        combo.activated[str].connect(self.onActivated)
        layout.addWidget(combo,1, 2) 
        #layout.addWidget(combo)
        lb_ch2 = QLabel("Plot2:") 
        lb_ch2.setFont(samllfont) 
        lb_ch2.move(20, 40)    
        layout.addWidget(lb_ch2,2, 1)  
        #layout.addWidget(lb_ch2)
        combo_ch2 = QComboBox(self)
        for i in range(26):
         combo_ch2.addItem(str(i))
        combo_ch2.activated[str].connect(self.onActivated_ch2)
        combo_ch2.move(80, 40)
        layout.addWidget(combo_ch2,2, 2) 

        #lb_ch3 = QLabel("CH 3:")  
        #lb_ch3.move(20, 70)      
        #layout.addWidget(lb_ch2)
        #combo_ch3 = QComboBox(self)
        #for i in range(26):
         #combo_ch3.addItem(str(i))
        #combo_ch3.activated[str].connect(self.onActivated_ch3)
        #combo_ch3.move(80, 70)

        self.digital_ch = 0
        str_dig = ["diff_wn_en",       
                   "delayed_trig",       
                   "beam_expected",      
                   "ena_charge",         
                   "ena_beam_width_meas",
                   "ena_flattop",        
                   "ena_lowthresh",      
                   "dis_errant",         
                   "ena_rise_1us_charge",
                   "ena_fall_1us_charge"]

        lb_ch4 = QLabel("Digital Ch:")  
        lb_ch4.move(20, 70)      
        lb_ch4.setFont(samllfont) 
        layout.addWidget(lb_ch4,3, 1)
        #layout.addWidget(lb_ch2)
        combo_ch4 = QComboBox(self)
        for i in range(9):
         combo_ch4.addItem(str_dig[i])
        combo_ch4.activated[int].connect(self.onActivated_ch4)
        combo_ch4.move(80, 100)
        layout.addWidget(combo_ch4,3, 2) 

        
        self.tim = []
        for j in range(0, 1024*32*4):        
          self.tim.append(j*8*11.35e-6)


        #layout.addWidget(combo_ch2)
        dynamic_canvas       = FigureCanvas(Figure(figsize=(5, 3)))
        #print(dynamic_canvas)
        layout.addWidget(dynamic_canvas, 4, 0, 18, 20)

        self.setLayout(layout)    
        
        self.setGeometry(300, 300, 1200, 1000)

        self.addToolBar(QtCore.Qt.BottomToolBarArea,
                        NavigationToolbar(dynamic_canvas, self))
        
        #dynamic_canvas.mpl_connect('button_press_event', self.on_press)
        #dynamic_canvas.mpl_connect('button_release_event', self.on_release)

        self._dynamic_ax = dynamic_canvas.figure.subplots(2)
        self.ax12 = self._dynamic_ax[0].twinx() 
        self.ax22 = self._dynamic_ax[1].twinx() 
#        self.ax32 = self._dynamic_ax[2].twinx() 


        self._timer = dynamic_canvas.new_timer(
            200, [(self._update_canvas, (), {})])
        self._timer.start()
   
    def on_release(self,event):
        if(self.temp_ydata<event.ydata and event.xdata<self.temp_xdata):
         self.y1 = self.y1_default
         self.y2 = self.y2_default
         self.x1 = self.x1_default
         self.x2 = self.x2_default
        else:
         self.y2 = self.temp_ydata
         self.x1 = self.temp_xdata
         self.y1 = event.ydata
         self.x2 = event.xdata
    def onplotBt(self):
        if(self.Plotrun == 1):
          self.Plotrun = 0; 
          self.plotBt.setText("Run");
        else:
          self.Plotrun = 1; 
          self.plotBt.setText("Stop");
        
    def onRst(self):
        #essffw.write_register(self.device, essbcm.bcm_common_registers['MPS_RESET'].offset,0x01)
        board_under_test['MPS_RESET'].write(0x01)

    def on_press(self, event):
        self.temp_ydata = event.ydata
        self.temp_xdata = event.xdata

    def onActivated(self,text):
        self.ch1 = int(text)
        print(text)

    def onActivated_ch2(self,text):
        self.ch2 = int(text)
        print(text)

    def onActivated_ch3(self,text):
        self.ch3 = int(text)
        print(text)

    def onActivated_ch4(self,index):
        self.digital_ch = 9-index

    def _update_canvas(self):
        
        #res = essffw.wait_for_acq_done(self.device);
        board_under_test.wait_acq()   

        ff_value_l = board_under_test['MEAS_CHARGE_FLATTOP_L_CH-0'].read()
        ff_value_h = board_under_test['MEAS_CHARGE_FLATTOP_H_CH-0'].read()
        ff_value = (ff_value_h<<32) + ff_value_l
        trig_flat_rising_t = board_under_test['BCM_FLATTOP_RISING_TIME_REG_CH-0'].read()
        trig_flat_falling_t = board_under_test['BCM_FLATTOP_FALLING_TIME_REG_CH-0'].read()
        trig_flat_wd = trig_flat_falling_t - trig_flat_rising_t
        if(ff_value >= 2**36):
           ff_value = ff_value-2**37
        ff_value = 1.0*ff_value*100.0/(2*16384.0*trig_flat_wd)

        self.cn = self.cn + 1
        self.current_avg = self.current_avg + ff_value
        if( self.cn == 20 ):
          self.log_file.write("%4.3f \r\n"%(self.current_avg/self.cn))
          self.current_avg = 0
          self.cn = 0;

        self.log_file.flush()
        
        self.lb_Cur.setText('Beam Current : {0:4.3f} mA '.format(ff_value))

        ff_value = board_under_test['MEAS_BEAMPULSE_WIDTH_CH-0'].read()
        self.lb_BW.setText('Beam Width : {0:4.3f} ms'.format(ff_value*11.356e-6))

        ff_value = board_under_test['MEAS_TRIG_WID'].read()
        self.lb_TW.setText('Trig Width : {0:4.3f} ms'.format(ff_value*11.356e-6))

        self.lb_FTW.setText('Flat top width : {0:4.3f} ms'.format(trig_flat_wd*11.356e-6))

        if(self.Plotrun == 0 ):
          return

        d = open(self.device, "rb")
        d.seek(0x1000000*self.ch1)
        data = d.read(0x80000)
        d.seek(0x1000000*self.ch2)
        data2 = d.read(0x80000)
        d.seek(0x1000000*self.ch3)
        data3 = d.read(0x80000)
        d.seek(0x1000000*23)
        data4 = d.read(0x80000)
        d.close()

        pd = np.frombuffer(data ,dtype='f')

        od = np.frombuffer(data2,dtype='f')
        diffd = np.frombuffer(data3,dtype='f')
        td = np.frombuffer(data4,dtype=np.uint32)
        z = np.copy(td)
        td2 = np.right_shift(z,self.digital_ch)
        td1 = np.bitwise_and(td2,0x01)

        if( self.first == 1):
         self.p1, = self._dynamic_ax[0].plot(self.tim, pd ,scalex=False, scaley=False,)
         
         self._dynamic_ax[0].set_ylim(self.y1,self.y2)
         self._dynamic_ax[0].set_xlim(self.x1,self.x2)
         self._dynamic_ax[0].set_ylabel('Isrc current (mA)')
         self._dynamic_ax[0].grid(True)
         self.p11, = self.ax12.plot(self.tim,td1,scalex=False, scaley=False , color = 'red')
         self.ax12.set_ylim(0,1)
         self.ax12.set_xlim(self.x1,self.x2)

         self.p2, =self._dynamic_ax[1].plot(self.tim, od ,scalex=False, scaley=False, color = 'g')
         self._dynamic_ax[1].set_ylim(self.y1,self.y2)
         self._dynamic_ax[1].set_xlim(self.x1,self.x2)
         self._dynamic_ax[1].set_ylabel('Leaky integrator (uQ)') #LEBT current(mA)')
         self._dynamic_ax[1].grid(True)
         self.p21, = self.ax22.plot(self.tim,td1,scalex=False, scaley=False , color = 'red')
         self.ax22.set_ylim(0,1)
         self.ax22.set_xlim(self.x1,self.x2)
         self._dynamic_ax[1].set_xlabel('Time (ms)') #LEBT current(mA)')
         self.first = 0;
        else:
         self.p1.set_ydata(pd)
         self.p11.set_ydata(td1)
         self.p2.set_ydata(od)
         self.p21.set_ydata(td1)
        self._dynamic_ax[1].figure.canvas.draw()

       
        
if __name__ == "__main__":
   print()
   print("****************************************************")
   print(" BCM Waveform Plot on SIS8300-KU")
   print("****************************************************")
   print()
   essffw_devices = essffw.find_essffw_devices()
   print("BCM ESSFFW-running devices found: {}".format(essffw_devices))
   if len(essffw_devices) < 1:
     print("Error: No BCM ESSFFW-running device found in system.")
     exit()
   board_under_test = essffw.Device(essffw.SIS8300Drv())
   board_under_test.add_register_bank(bcm.bcm_fw_registers)
   board_under_test.add_register_bank(bcm.bcm_com_registers)
   board_under_test.add_register_bank(bcm.bcm_acct_registers)
   board_under_test.add_register_bank(bcm.bcm_dod_registers)
   board_under_test.add_register_bank(bcm.bcm_diff_registers)
   board_under_test.add_register_bank(bcm.bcm_fiber_registers)
   board_under_test.add_register_bank(bcm.bcm_probe_registers)
   board_under_test.connect(essffw_devices[0])

   print("****************************************************")
   print(" Set the clock source to the backplane over EVR")
   print("****************************************************")
   board_under_test['CLOCK_SRC'].write(0x0a)
   qapp = QtWidgets.QApplication(sys.argv)
   app = ApplicationWindow()
   app.show()
   qapp.exec_()

