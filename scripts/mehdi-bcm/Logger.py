import subprocess
import threading
import time
from time import sleep
import essffw
import essbcm

def run():
    oldMode = -1;
    log_file = open("logtemp.txt","w+");
    cal_file = open("logcal.txt","w+");
    device = "/dev/sis8300-5" 
    temp = -100.0
    start_time = time.time()
    calib_cn = 59#599
    calib_done = 0
    drop_ch2 = 0.0;
    while True:
      sleep(1)
            
      calib_cn = calib_cn + 1
      calib_done = 0
      if( calib_cn == 60 ):
         essffw.write_register(device, essbcm.bcm_common_registers['CTRL_ENABLE_CALIBRATION_PULSE'].offset,  0x01) 
         sleep(1)
         point1 = essffw.read_register(device, essbcm.adc_registers['CAL_SAMPLE1'].offset+0x500)
         point2 = essffw.read_register(device, essbcm.adc_registers['CAL_SAMPLE2'].offset+0x500)
         point3 = essffw.read_register(device, essbcm.adc_registers['CAL_SAMPLE3'].offset+0x500)
         point4 = essffw.read_register(device, essbcm.adc_registers['CAL_SAMPLE4'].offset+0x500)
         cal_file.write("%d, %d, %d, %d \r\n"%(point1,point2,point3,point4))
         cal_file.flush()
         droop = 32768*100*(point2 - point3)/(1.0*2.1*2.4*point2)

         #print(droop)
         if(point1 >= 0x8000):
            point1 = point1 - 0xffff
         diff = point2-point1
         delta = (point2 - point3)*49.665/(diff)
         scale = int(32768*16384.0*49.665/(100*diff));
         scale_old = essffw.read_register(device, essbcm.adc_registers['ADC_SCALE'].offset+0x500)
         if (scale_old == 0):
           scale_old = scale;
         scale = int((scale + scale_old)/2.0); 
         essffw.write_register(device, essbcm.adc_registers['ADC_SCALE'].offset+0x500,  scale) 
         essffw.write_register(device, essbcm.adc_registers['ADC_DROOPRATE'].offset+0x500,  int(droop) )
         calib_cn = 0
         calib_done = 1
      try:   
          result = subprocess.check_output(["caput", "ETHMOD:I2C1:Temp3:Read", "1"])
          result = subprocess.check_output(["caget", "ETHMOD:I2C1:Temp3:StatusMessage_RBV"]).decode()
          s = result.split(' ')
          if ( s[2] == '82' and s[3] == '101'):
            result = subprocess.check_output(["caget", "ETHMOD:I2C1:Temp3:Value_RBV"]).decode()
            s2 = result.split(' ')
            print(float(s2[4]))
            temp = float(s2[4])
      except subprocess.CalledProcessError as e:
           print(e.output)
      ff_value_l = essffw.read_register(device, essbcm.adc_registers['MEAS_CHARGE_FLATTOP_L'].offset+0x500)
      ff_value_h = essffw.read_register(device, essbcm.adc_registers['MEAS_CHARGE_FLATTOP_H'].offset+0x500)
      ff_value = (ff_value_h<<32) + ff_value_l

      ff_value_l_ch2 = essffw.read_register(device, essbcm.adc_registers['MEAS_CHARGE_FLATTOP_L'].offset+0x600)
      ff_value_h_ch2 = essffw.read_register(device, essbcm.adc_registers['MEAS_CHARGE_FLATTOP_H'].offset+0x600)
      ff_value_ch2 = (ff_value_h_ch2<<32) + ff_value_l_ch2

      drop1_ch1 = essffw.read_register(device, essbcm.adc_registers['BCM_MEAS_CHARGE_RISING'].offset+0x500)*100.0/(2*87*16384.0)
      drop2_ch1 = essffw.read_register(device, essbcm.adc_registers['BCM_MEAS_CHARGE_FALLING'].offset+0x500)*100.0/(2*87*16384.0)
      drop_ch1 = 100.0*(drop1_ch1 - drop2_ch1)/(drop1_ch1*2.8)
      #print(drop1_ch1,drop2_ch1,drop_ch1)
      drop1_ch2 = essffw.read_register(device, essbcm.adc_registers['BCM_MEAS_CHARGE_RISING'].offset+0x600)*100.0/(2*87*16384.0)
      drop2_ch2 = essffw.read_register(device, essbcm.adc_registers['BCM_MEAS_CHARGE_FALLING'].offset+0x600)*100.0/(2*87*16384.0)
      if(drop1_ch2>0):
        drop_ch2 = 100.0*(drop1_ch2 - drop2_ch2)/(drop1_ch2*2.8)


      trig_flat_wd = essffw.read_register(device, essbcm.bcm_common_registers['MEAS_TIME_FLATTOP'].offset)
      if(ff_value >= 2**36):
          ff_value = ff_value-2**37
      cur_value = 1.0*ff_value*100.0/(2*16384.0*trig_flat_wd)

      if(ff_value_ch2 >= 2**36):
          ff_value_ch2 = ff_value_ch2-2**37
      cur_value_ch2 = 1.0*ff_value_ch2*100.0/(2*16384.0*trig_flat_wd)

      print (cur_value,cur_value_ch2,drop_ch1,drop_ch2)
      elapsed_time = time.time() - start_time;
      if( temp > -100 ): 
        log_file.write("%4.2f, %4.3f, %4.3f, %d , %4.3f, %4.3f , %4.3f , %4.3f, %4.3f\r\n"%(elapsed_time,temp,cur_value,calib_done,droop,delta,cur_value_ch2,drop_ch1,drop_ch2))
        log_file.flush()
    log_file.close()
t = threading.Thread(target=run)
t.start()
