#import essbcm
#import essffw
import csv
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit, QPushButton, QDialog, QVBoxLayout, QComboBox
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import time
import math
import os
import sys
import inspect
import struct

# Load modules from other folders
import_folder = "ess-bcm-struck-sis8300-ku/sw/python/bcm"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import bcm

import_folder = "ess-bcm-struck-sis8300-ku/lib/hw/ess_fpga_framework/sw/python"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import essffw


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'BCM Tester'
        self.left = 10
        self.top = 10
        self.width = 300
        self.height = 200
        self.device = "/dev/sis8300-4"
        self.bcm_cm_dlg    = None
        self.bcm_adc_dlg   = None
        self.bcm_diff_dlg  = None
        self.bcm_fiber_dlg = None
        self.bcm_probe_dlg = None
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)


        bcm_cm =   QPushButton('BCM COM Reg', self)		
        
        bcm_cm.move(10 ,10)
        bcm_cm.resize(200,20)
        bcm_cm.clicked.connect(self.on_bcm_cm)

        bcm_adc =  QPushButton('BCM ADC Ch Reg', self)		
        bcm_adc.move(10 ,40)
        bcm_adc.resize(200,20)
        bcm_adc.clicked.connect(self.on_bcm_adc)

        bcm_diff = QPushButton('BCM Diff Ch Reg', self)		
        #self.bcm_cm_wnd = bcm_common_wnd(self.new , self.device)
        bcm_diff.move(10 ,70)
        bcm_diff.resize(200,20)
        bcm_diff.clicked.connect(self.on_bcm_diff)

        bcm_fb =   QPushButton('BCM Fiber Ch Reg', self)		
        #self.bcm_cm_wnd = bcm_common_wnd(self.new , self.device)
        bcm_fb.move(10 ,100)
        bcm_fb.resize(200,20)
        bcm_fb.clicked.connect(self.on_bcm_fb)

        bcm_pb =   QPushButton('BCM Probe Ch Reg', self)		
        #self.bcm_cm_wnd = bcm_common_wnd(self.new , self.device)
        bcm_pb.move(10 ,130)
        bcm_pb.resize(200,20)
        bcm_pb.clicked.connect(self.on_bcm_pb)

        self.show()
       


    @pyqtSlot()
    def on_bcm_cm(self):
       bcm_cm_wnd = bcm_common_wnd(self , self.device)
       bcm_cm_wnd.exec_()
       
    @pyqtSlot()
    def on_bcm_adc(self):
       adc_wnd = bcm_adc_wnd(self , self.device)
       adc_wnd.exec_()

    @pyqtSlot()
    def on_bcm_diff(self):
       diff_wnd = bcm_diff_wnd(self , self.device)
       diff_wnd.exec_()

    @pyqtSlot()
    def on_bcm_fb(self):
       fb_wnd = bcm_fiber_wnd(self , self.device)
       fb_wnd.exec_()

    @pyqtSlot()
    def on_bcm_pb(self):
       pb_wnd = bcm_probe_wnd(self , self.device)
       pb_wnd.exec_()


class Dialog(QDialog):
 
    def slot_method(self):
        print('slot method called.')
 
    def __init__(self):
        super(Dialog, self).__init__()
 
        button=QPushButton("Click")
        button.clicked.connect(self.slot_method)
 
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(button)
 
        self.setLayout(mainLayout)
        self.setWindowTitle("Button Example - pythonspot.com")
        self.show()

class  bcm_adc_wnd(QDialog):

    def __init__(self,wnd, device):
        super(bcm_adc_wnd,self).__init__()
        self.setWindowTitle("BCM ADC CH Registers")
        self.device = device
        lbl_wd = 1
        self.txtb = {}
        self.txtbw = {}
        GetBtn = QPushButton('Read Reg', self)
        GetBtn.move(700,20) 
        GetBtn.clicked.connect(self.read_reg)
  
        SetBtn = QPushButton('Write Reg', self)
        SetBtn.move(800,20) 
        SetBtn.clicked.connect(self.write_reg)
        
        combolb = QLabel("ADC CH:",self);
        combolb.move(10, 0)

        self.chcb = QComboBox(self) 
        self.chcb.move(100, 0)

        for i in range(10):
         self.chcb.addItem(str(i))


        halarm = QLabel('HAlarm',self)     
        halarm.move(800, 50)

        halarm = QLabel('FAlarm',self)     
        halarm.move(850, 50)

        halarm = QLabel('DAlarm',self)     
        halarm.move(900, 50)

        alarm0 = QLabel('High Pulse Amp.',self)     
        alarm0.move(600, 70)

        alarm1 = QLabel('Low Pulse Amp.',self)     
        alarm1.move(600, 90)

        alarm2 = QLabel('Errant beam',self)     
        alarm2.move(600, 110)

        alarm3 = QLabel('beam longer than trig.',self)     
        alarm3.move(600, 130)

        alarm4 = QLabel('beam pulse longer than limit param.',self)     
        alarm4.move(600, 150)

        alarm5 = QLabel('ADC overflow',self)     
        alarm5.move(600, 170)

        alarm6 = QLabel('ADC underflow',self)     
        alarm6.move(600, 190)

        alarm7 = QLabel('ADC stuck',self)     
        alarm7.move(600, 210)


        self.hal = {}
        self.fal = {}
        self.dal = {}
        for i in range(8):
            lb = QLabel(self)
            lb.move(800,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.hal[i] = lb
            lb = QLabel(self)
            lb.move(850,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.fal[i] = lb 
            lb = QLabel(self)
            lb.move(900,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.dal[i] = lb 



        for addr, RegBank in board_under_test.register_banks.items():
          if(RegBank.name == 'BCM_ACCT'):
            for register in RegBank.get_registers():  
               if( RegBank.get_address(register.name) < 0x540 and RegBank.get_address(register.name) >= 0x500):
                 reg_name = ((register.name).split('_CH-'))[0]
                 lbl = QLabel(self)
                 self.txtb[reg_name] = QLineEdit(self); 
                 self.txtb[reg_name].resize(150,20)
                 self.txtb[reg_name].setReadOnly(True);
                 lbl.setText(reg_name + ':')
                 lbl.adjustSize()
                 lbl.move(10,20*lbl_wd)
                 self.txtb[reg_name].move(300,20*lbl_wd)
                 if ( register.modes == {'R','W'} or register.modes == {'W'}):
                   self.txtbw[reg_name] = QLineEdit(self); 
                   self.txtbw[reg_name].move(450,20*lbl_wd)
                   #self.txtbw[reg_name].setText('0x{0}'.format(reg_info.reset_value))
                 lbl_wd = lbl_wd+1

    @pyqtSlot()
    def write_reg(self): 
        ch = int(self.chcb.currentText()) 
        for reg_name in self.txtbw.keys():
            tstr = self.txtbw[reg_name].text() 
            if ( tstr == ""):
               value = 0
            else:
               value = int(tstr,0)
            board_under_test[reg_name+'_CH-'+str(ch)].write(value)
            
    @pyqtSlot()
    def read_reg(self):
      ch = int(self.chcb.currentText()) + 1;
      for addr, RegBank in board_under_test.register_banks.items():
       if(RegBank.name == 'BCM_ACCT'):
        for register in RegBank.get_registers():  
         if( RegBank.get_address(register.name) < 0x540 and RegBank.get_address(register.name) >= 0x500):
          reg_name = ((register.name).split('_CH-'))[0]   
          ff_value = board_under_test[reg_name+'_CH-'+str(ch-1)].read() 
          if(reg_name == 'MEAS_BEAMPULSE_WIDTH'):  
            self.txtb[reg_name].setText('0x{0:x}: {1:4.3f} ms'.format(ff_value,int(ff_value)/88052.0))
          else:
            self.txtb[reg_name].setText('0x{0:x}'.format(ff_value))
          if ( register.modes == {'R','W'} or register.modes == {'W'}):
               self.txtbw[reg_name].setText('0x{0:x}'.format(ff_value))
          if(reg_name == 'ADC_UPPER_THRESHOLD'):  
             self.txtb[reg_name].setText('(0x{0:x}): {1:4.3f} mA'.format(ff_value,ff_value*100.0/32768.0))
          if(reg_name == 'ADC_LOWER_THRESHOLD'):  
             self.txtb[reg_name].setText('(0x{0:x}): {1:4.3f} mA'.format(ff_value,ff_value*100.0/32768.0))
          if(reg_name == 'ADC_ERRANT_THRESHOLD'):  
             self.txtb[reg_name].setText('(0x{0:x}): {1:4.3f} mA'.format(ff_value,ff_value*100.0/32768.0))
          if(reg_name == 'ADC_MAX_PULSE_LENGTH'):  
             self.txtb[reg_name].setText('(0x{0:x}): {1:4.3f} ms'.format(ff_value,ff_value*1.01015/1000.0))             
          if(reg_name == 'ADC_MPS_ALARM_HOLD'):  
              for inx in range(8):
                  if(ff_value & (1<<inx)):
                     self.hal[inx].setStyleSheet('background-color: red')
                  else:
                     self.hal[inx].setStyleSheet('background-color: black')
          if(reg_name == 'ADC_MPS_ALARM_FIRST'):  
              for inx in range(8):
                  if(ff_value & (1<<inx)):
                     self.fal[inx].setStyleSheet('background-color: red')
                  else:
                     self.fal[inx].setStyleSheet('background-color: black')
          if(reg_name == 'ADC_MPS_ALARM_DIRECT'):  
              for inx in range(8):
                  if(ff_value & (1<<inx)):
                     self.dal[inx].setStyleSheet('background-color: red')
                  else:
                     self.dal[inx].setStyleSheet('background-color: black')


class  bcm_diff_wnd(QDialog):

    def __init__(self,wnd, device):
        super(bcm_diff_wnd,self).__init__()
        self.setWindowTitle("BCM DIFF CH Registers")
        self.device = device
        lbl_wd = 1
        self.txtb = {}
        self.txtbw = {}
        GetBtn = QPushButton('Read Reg', self)
        GetBtn.move(700,20) 
        GetBtn.clicked.connect(self.read_reg)
  
        SetBtn = QPushButton('Write Reg', self)
        SetBtn.move(800,20) 
        SetBtn.clicked.connect(self.write_reg)
        
        combolb = QLabel("DIFF CH:",self);
        combolb.move(10, 0)

        self.chcb = QComboBox(self) 
        self.chcb.move(100, 0)
        for i in range(10):
         self.chcb.addItem(str(i))


        halarm = QLabel('HAlarm',self)     
        halarm.move(800, 50)

        halarm = QLabel('FAlarm',self)     
        halarm.move(850, 50)

        halarm = QLabel('DAlarm',self)     
        halarm.move(900, 50)

        alarm0 = QLabel('Fast Interlock ',self)     
        alarm0.move(700, 70)

        alarm1 = QLabel('Medium Interlock ',self)     
        alarm1.move(700, 90)

        alarm2 = QLabel('Slow Interlock ',self)     
        alarm2.move(700, 110)


        self.hal = {}
        self.fal = {}
        self.dal = {}
        for i in range(3):
            lb = QLabel(self)
            lb.move(800,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.hal[i] = lb
            lb = QLabel(self)
            lb.move(850,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.fal[i] = lb 
            lb = QLabel(self)
            lb.move(900,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.dal[i] = lb 


        for addr, RegBank in board_under_test.register_banks.items():
          if(RegBank.name == 'BCM_DIFF'):
            for register in RegBank.get_registers():  
               if( RegBank.get_address(register.name) < 0x580 and RegBank.get_address(register.name) >= 0x540):
                 lbl = QLabel(self)
                 Rname = (register.name).split('_CH-')
                 reg_name = Rname[0]
                 self.txtb[reg_name] = QLineEdit(self); 
                 self.txtb[reg_name].resize(150,20)
                 self.txtb[reg_name].setReadOnly(True);
                 lbl.setText(reg_name + ':')
                 lbl.adjustSize()
                 lbl.move(10,20*lbl_wd)
                 self.txtb[reg_name].move(300,20*lbl_wd)
                 if ( register.modes == {'R','W'} or register.modes == {'W'}):
                    self.txtbw[reg_name] = QLineEdit(self); 
                    self.txtbw[reg_name].move(500,20*lbl_wd)
                    #self.txtbw[register.name].setText('0x{0}'.format(reg_info.reset_value))
                 lbl_wd = lbl_wd+1
    

    @pyqtSlot()
    def write_reg(self): 
        ch = int(self.chcb.currentText());
        for reg_name in self.txtbw.keys():
            txt = self.txtbw[reg_name].text() 
            if ( txt == ""):
               value = 0
            else:
               value = int(txt,0)
            #essffw.write_register(self.device, essbcm.diff_registers[reg_name].offset+0x440+0x100*ch,  value) 
            board_under_test[reg_name+'_CH-'+str(ch)].write(value)
    @pyqtSlot()
    def read_reg(self):
      ch = int(self.chcb.currentText()) ;
      for addr, RegBank in board_under_test.register_banks.items():
       if(RegBank.name == 'BCM_DIFF'):
        for register in RegBank.get_registers():   
         if( RegBank.get_address(register.name) < 0x580 and RegBank.get_address(register.name) >= 0x540):
          reg_name = ((register.name).split('_CH-'))[0]
          ff_value = board_under_test[reg_name+'_CH-'+str(ch)].read()
          hex_value = ff_value
          self.txtb[reg_name].setText('0x{0:x}'.format(ff_value))
          if(register.modes == {'R','W'}):
            self.txtbw[reg_name].setText('0x{0:x}'.format(ff_value))

          if(reg_name == 'DIFF_FAST_THRESH'):  
              self.txtb[reg_name].setText('(0x{0:x}):{1:4.3f} mA'.format(ff_value,ff_value*100.0/32768))

          if(reg_name == 'DIFF_MEDIUM_THRESH'):  
              self.txtb[reg_name].setText('(0x{0:x}):{1:4.3f} mA'.format(ff_value,ff_value*100.0/32768))

          if(reg_name == 'DIFF_FAST_MAX'):  
              if(ff_value>0x80000000):
                 ff_value = ff_value - 0xffffffff
              self.txtb[reg_name].setText('(0x{0:x}):{1:4.3f} mA'.format(hex_value,ff_value*100.0/32768))
          if(reg_name == 'DIFF_MEDIUM_MAX'):  
              if(ff_value>0x80000000):
                 ff_value = ff_value - 0xffffffff
              self.txtb[reg_name].setText('(0x{0:x}):{1:4.3f} mA'.format(hex_value,ff_value*100.0/32768))

          if(reg_name == 'DIFF_FAST_MIN'):  
              
              if(ff_value>0x80000000):
                 ff_value = ff_value - 0xffffffff
              self.txtb[reg_name].setText('(0x{0:x}):{1:4.3f} mA'.format(hex_value,ff_value*100.0/32768))

          if(reg_name == 'DIFF_MEDIUM_MIN'):  
              if(ff_value>0x80000000):
                 ff_value = ff_value - 0xffffffff
              self.txtb[reg_name].setText('(0x{0:x}):{1:4.3f} mA'.format(hex_value,ff_value*100.0/32768))

          if(reg_name == 'DIFF_MPS_ALARM_HOLD'):  
              for inx in range(3):
                  if(ff_value & (1<<inx)):
                     self.hal[inx].setStyleSheet('background-color: red')
                  else:
                     self.hal[inx].setStyleSheet('background-color: black')
          if(reg_name == 'DIFF_MPS_ALARM_FIRST'):  
              for inx in range(3):
                  if(ff_value & (1<<inx)):
                     self.fal[inx].setStyleSheet('background-color: red')
                  else:
                     self.fal[inx].setStyleSheet('background-color: black')
          if(reg_name == 'DIFF_MPS_ALARM_DIRECT'):  
              for inx in range(3):
                  if(ff_value & (1<<inx)):
                     self.dal[inx].setStyleSheet('background-color: red')
                  else:
                     self.dal[inx].setStyleSheet('background-color: black')




class  bcm_fiber_wnd(QDialog):

    def __init__(self,wnd, device):
        super(bcm_fiber_wnd,self).__init__()
        self.setWindowTitle("BCM FIBER CH Registers")
        self.device = device
        lbl_wd = 1
        self.txtb = {}
        self.txtbw = {}
        GetBtn = QPushButton('Read Reg', self)
        GetBtn.move(700,20) 
        GetBtn.clicked.connect(self.read_reg)
  
        SetBtn = QPushButton('Write Reg', self)
        SetBtn.move(800,20) 
        SetBtn.clicked.connect(self.write_reg)

        combolb = QLabel("Fiber CH:",self);
        combolb.move(10, 0)

        self.chcb = QComboBox(self) 
        self.chcb.move(100, 0)
        
        for i in range(2):
         self.chcb.addItem(str(i))

        for addr, RegBank in board_under_test.register_banks.items():
          if(RegBank.name == 'BCM_FIBER'):
            for register in RegBank.get_registers():  
               if( RegBank.get_address(register.name) < 0x5C0 and RegBank.get_address(register.name) >= 0x580):
                 reg_name = ((register.name).split('_CH-'))[0]
                 lbl = QLabel(self)
                 self.txtb[reg_name] = QLineEdit(self); 
                 self.txtb[reg_name].resize(150,20)
                 self.txtb[reg_name].setReadOnly(True);
                 lbl.setText(reg_name + ':')
                 lbl.adjustSize()
                 lbl.move(10,20*lbl_wd)
                 self.txtb[reg_name].move(300,20*lbl_wd)
                 if ( register.modes == {'R','W'} or register.modes == {'W'}):
                   self.txtbw[reg_name] = QLineEdit(self); 
                   self.txtbw[reg_name].move(500,20*lbl_wd)
                   #self.txtbw[reg_name].setText('0x{0}'.format(reg_info.reset_value))
                 lbl_wd = lbl_wd+1

    @pyqtSlot()
    def write_reg(self): 
        ch = int(self.chcb.currentText()) 
        for reg_name in self.txtbw.keys():
            tstr = self.txtbw[reg_name].text() 
            if ( tstr == ""):
               value = 0
            else:
               value = int(tstr,0)
            board_under_test[reg_name+'_CH-'+str(ch)].write(value) 
    @pyqtSlot()
    def read_reg(self):
       ch = int(self.chcb.currentText()) ;
       for reg_name in self.txtb.keys():
          ff_value = board_under_test[reg_name+'_CH-'+str(ch)].read() 
          self.txtb[reg_name].setText('0x{0:x}'.format(ff_value))



class  bcm_probe_wnd(QDialog):

    def __init__(self,wnd, device):
        super(bcm_probe_wnd,self).__init__()
        self.setWindowTitle("BCM PROBE CH Registers")
        self.device = device
        lbl_wd = 1
        self.txtb = {}
        self.txtbw = {}
        GetBtn = QPushButton('Read Reg', self)
        GetBtn.move(700,20) 
        GetBtn.clicked.connect(self.read_reg)
  
        SetBtn = QPushButton('Write Reg', self)
        SetBtn.move(800,20) 
        SetBtn.clicked.connect(self.write_reg)
        self.reg_name_base = 'PROBE_SOURCE_SELECT'
        for i in range(4):
             lbl = QLabel(self)
             reg_name = self.reg_name_base+'_CH-{0}'.format(i)
             self.txtb[reg_name] = QLineEdit(self); 
             self.txtb[reg_name].resize(150,20)
             self.txtb[reg_name].setReadOnly(True);
             lbl.setText(reg_name + ':')
             lbl.adjustSize()
             lbl.move(10,20*lbl_wd)
             self.txtb[reg_name].move(300,20*lbl_wd)
             self.txtbw[reg_name] = QLineEdit(self); 
             self.txtbw[reg_name].move(500,20*lbl_wd)
             self.txtbw[reg_name].setText('0x0')
             lbl_wd = lbl_wd+1

    @pyqtSlot()
    def write_reg(self): 
        for ch in range(4):
            reg_name = self.reg_name_base+'_CH-{0}'.format(ch)
            tstr = self.txtbw[reg_name].text() 
            if ( tstr == ""):
               value = 0
            else:
               value = int(tstr,0)
            board_under_test[reg_name].write(value) 
            #essffw.write_register(self.device, essbcm.probe_registers[self.reg_name_base].offset+0x4C0+0x100*(ch+1),  value) 
    @pyqtSlot()
    def read_reg(self):
        for ch in range(4):
          reg_name = self.reg_name_base+'_CH-{0}'.format(ch)
          ff_value = board_under_test[reg_name].read() #essffw.read_register(self.device, essbcm.probe_registers[self.reg_name_base].offset+0x4C0+0x100*(ch+1))
          #print(reg_name+ ': 0x{0:x}'.format(ff_value))
          self.txtb[reg_name].setText('0x{0:x}'.format(ff_value))


   
class  bcm_common_wnd(QDialog):
    def acq_ch_setting(no_samples, ch_offset):
    #------------------------------------------------------------------------------
    #-- Ch recorder setting
    #------------------------------------------------------------------------------
      offset = 0x0
      for ch in range(20):
         board_under_test['ACQ_CH_NO'].write(ch) 
         board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
         board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write(ch_offset*ch+offset) 
         board_under_test['NUM_FRAC_BIT_REG'].write(0xf) 
         board_under_test['SCALE_FACT_REG'].write(0xc8<<15) #(0X5a<<15) + 0x2766) 
         board_under_test['OFFSET_FACT_REG'].write( 0x0) 
         board_under_test['DEC_FACT_REG'].write(0x8) 
         board_under_test['FP_SCALE_REC_EN_REG'].write(0x0) 

      for ch in range(20,21):
         board_under_test['ACQ_CH_NO'].write(ch) 
         board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
         board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write(ch_offset*ch+offset) 
         board_under_test['NUM_FRAC_BIT_REG'].write( 0xf) 
         board_under_test['SCALE_FACT_REG'].write(0xc8<<15 ) #(0X5a<<15) + 0x2766) 
         board_under_test['OFFSET_FACT_REG'].write(0x0) 
         board_under_test['DEC_FACT_REG'].write( 0x8) 
         board_under_test['FP_SCALE_REC_EN_REG'].write(0x0) # 0x07 

      ch = 22 
      board_under_test['ACQ_CH_NO'].write( ch) 
      board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
      board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write( ch_offset*ch+offset) 
      board_under_test['NUM_FRAC_BIT_REG'].write( 0xf) 
      board_under_test['SCALE_FACT_REG'].write( 0x8df6666 ) #(0X5a<<15) + 0x2766) 
      board_under_test['OFFSET_FACT_REG'].write( 0x0) 
      board_under_test['DEC_FACT_REG'].write( 0x8) 
      board_under_test['FP_SCALE_REC_EN_REG'].write(0x0) # 0x07 


      ch = 23 
      board_under_test['ACQ_CH_NO'].write( ch) 
      board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
      board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write(ch_offset*ch+offset) 
      board_under_test['NUM_FRAC_BIT_REG'].write( 0xf) 
      board_under_test['SCALE_FACT_REG'].write(0xc8<<15 ) #(0X5a<<15) + 0x2766) 
      board_under_test['OFFSET_FACT_REG'].write( 0x0) 
      board_under_test['DEC_FACT_REG'].write( 0x8) 
      board_under_test['FP_SCALE_REC_EN_REG'].write(0x07) # 0x07 
    def LUTs(self):
        with open('BCM_config.csv') as csv_file:
         csv_reader = csv.reader(csv_file, delimiter=',')
         line_count = 0
         for row in csv_reader:
            addr = 0x20 + (int(row[0],16)<<6) + int(row[1],16)
            essbcm.acct_lut(self.device, addr , int(row[2],16))

            addr = 0x10 + (int(row[0],16)<<6) + int(row[1],16)
            essbcm.acct_lut(self.device, addr , int(row[3],16))

            addr = 0x0 + (int(row[0],16)<<6) + int(row[1],16)
            essbcm.acct_lut(self.device, addr , int(row[4],16))
            print(int(row[4],16))
            line_count += 1
        print(f'Processed {line_count} lines.')

        with open('BCM_exist.csv') as csv_file:
         csv_reader = csv.reader(csv_file, delimiter=',')
         line_count = 0
         for row in csv_reader:
            addr = 0x400 + int(row[0],16)
            essbcm.acct_lut(self.device, addr , int(row[1],16))

    #------------------------------------------------------------------------------
    #-- Ch recorder setting
    #------------------------------------------------------------------------------
    def acq_reg(self):
      #  self.acq_ch_setting(0x20000, 0x1000000)
      ch_offset = 0x1000000
      no_samples= 0x20000
      offset = 0x0
      for ch in range(20):
         board_under_test['ACQ_CH_NO'].write(ch) 
         board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
         board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write(ch_offset*ch+offset) 
         board_under_test['NUM_FRAC_BIT_REG'].write(0xf) 
         board_under_test['SCALE_FACT_REG'].write(0xc8<<15) #(0X5a<<15) + 0x2766) 
         board_under_test['OFFSET_FACT_REG'].write( 0x0) 
         board_under_test['DEC_FACT_REG'].write(0x8) 
         board_under_test['FP_SCALE_REC_EN_REG'].write(0x0) 

      for ch in range(20,21):
         board_under_test['ACQ_CH_NO'].write(ch) 
         board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
         board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write(ch_offset*ch+offset) 
         board_under_test['NUM_FRAC_BIT_REG'].write( 0xf) 
         board_under_test['SCALE_FACT_REG'].write(0xc8<<15 ) #(0X5a<<15) + 0x2766) 
         board_under_test['OFFSET_FACT_REG'].write(0x0) 
         board_under_test['DEC_FACT_REG'].write( 0x8) 
         board_under_test['FP_SCALE_REC_EN_REG'].write(0x0) # 0x07 

      ch = 22 
      board_under_test['ACQ_CH_NO'].write( ch) 
      board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
      board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write( ch_offset*ch+offset) 
      board_under_test['NUM_FRAC_BIT_REG'].write( 0xf) 
      board_under_test['SCALE_FACT_REG'].write( 0x8df6666 ) #(0X5a<<15) + 0x2766) 
      board_under_test['OFFSET_FACT_REG'].write( 0x0) 
      board_under_test['DEC_FACT_REG'].write( 0x8) 
      board_under_test['FP_SCALE_REC_EN_REG'].write(0x0) # 0x07 


      ch = 23 
      board_under_test['ACQ_CH_NO'].write( ch) 
      board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
      board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write(ch_offset*ch+offset) 
      board_under_test['NUM_FRAC_BIT_REG'].write( 0xf) 
      board_under_test['SCALE_FACT_REG'].write(0xc8<<15 ) #(0X5a<<15) + 0x2766) 
      board_under_test['OFFSET_FACT_REG'].write( 0x0) 
      board_under_test['DEC_FACT_REG'].write( 0x8) 
      board_under_test['FP_SCALE_REC_EN_REG'].write(0x07) # 0x07 

      print('ACQ setting Done')
    
    def __init__(self,wnd, device):
        super(bcm_common_wnd, self).__init__()
        self.setWindowTitle("BCM Common Registers")
        self.device = device
        lbl_wd = 1
        self.txtb = {}
        self.txtbw = {}
        GetBtn = QPushButton('Read Reg',self)
        GetBtn.move(700,20) 
        GetBtn.clicked.connect(self.read_reg)
  
        SetBtn = QPushButton('Write Reg',self)
        SetBtn.move(800,20) 
        SetBtn.clicked.connect(self.write_reg)
      
        AcqBtn = QPushButton('Acq Ch setting',self)
        AcqBtn.move(900,20) 
        AcqBtn.clicked.connect(self.acq_reg)
        #self.connect(AcqBtn, SIGNAL('clicked()'), self.acq_reg)

        lutBtn = QPushButton('Load LUTs',self)
        lutBtn.move(1000,20) 
        lutBtn.clicked.connect(self.LUTs)





        halarm = QLabel('HAlarm',self)     
        halarm.move(800, 50)

        halarm = QLabel('FAlarm',self)     
        halarm.move(850, 50)

        halarm = QLabel('DAlarm',self)     
        halarm.move(900, 50)

        alarm0 = QLabel('CLK Aux',self)     
        alarm0.move(700, 70)

        alarm1 = QLabel('Proc. CLK',self)     
        alarm1.move(700, 90)

        alarm2 = QLabel('Long Trig',self)     
        alarm2.move(700, 110)

        alarm3 = QLabel('Narrow Trig',self)     
        alarm3.move(700, 130)

        alarm4 = QLabel('Trig Period',self)     
        alarm4.move(700, 150)

        self.hal = {}
        self.fal = {}
        self.dal = {}
        for i in range(5):
            lb = QLabel(self)
            lb.move(800,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.hal[i] = lb
            lb = QLabel(self)
            lb.move(850,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.fal[i] = lb 
            lb = QLabel(self)
            lb.move(900,70+i*20) 
            lb.resize(15,15)
            lb.setStyleSheet('background-color: black')
            self.dal[i] = lb 

        for addr, RegBank in board_under_test.register_banks.items():
          if(RegBank.name == 'BCM_COM'):
            for register in RegBank.get_registers():  
              lbl = QLabel(self)
              self.txtb[register.name] = QLineEdit(self); 
              self.txtb[register.name].resize(150,20)
              self.txtb[register.name].setReadOnly(True);
              lbl.setText(register.name + ':')
              lbl.adjustSize()
              lbl.move(10,20*lbl_wd)
              self.txtb[register.name].move(300,20*lbl_wd)
              if ( register.modes == {'R','W'} or register.modes == {'W'}):
               if( RegBank.get_address(register.name) <= 0x426 or RegBank.get_address(register.name) > 0x42E):
                self.txtbw[register.name] = QLineEdit(self); 
                self.txtbw[register.name].move(500,20*lbl_wd)
                #self.txtbw[register.name].setText('0x{0}'.format(reg_info.reset_value))
              lbl_wd = lbl_wd+1
       

    def write_reg(self): 
        for reg_name in self.txtbw.keys():
            str = self.txtbw[reg_name].text() 
            if ( str == ""):
               value = 0
            else:
               value = int(str,0)
            board_under_test[reg_name].write(value) 

    def read_reg(self):
      for addr, RegBank in board_under_test.register_banks.items():
       if(RegBank.name == 'BCM_COM'):
        for register in RegBank.get_registers():  
          reg_name = register.name
          ff_value = board_under_test[reg_name].read()
          if ( register.modes == {'R','W'} or register.modes == {'W'}):
             if( RegBank.get_address(register.name) <= 0x426 or RegBank.get_address(register.name) > 0x42E):	
               self.txtbw[reg_name].setText('0x{0:x}'.format(ff_value))
          if(reg_name == 'MEAS_CLK_FREQ_KHZ'):
            if(ff_value != 0):
              proc_period = 1.0/(ff_value);
            else:
              proc_period = 0;
            self.txtb[reg_name].setText('{0} KHz'.format(ff_value))
          elif(reg_name == 'MEAS_TRIG_PERIOD'):
            if((ff_value != 0) and (proc_period != 0)):
              self.txtb[reg_name].setText('{0:4.3f} ms ({1:4.3f}Hz)'.format(ff_value*proc_period,1000.0/(ff_value*proc_period)))
          elif(reg_name == 'MEAS_TRIG_WID'):
            self.txtb[reg_name].setText('{0:4.3f} ms '.format(ff_value*proc_period))
          elif(reg_name == 'MEAS_TIME_FLATTOP'):
            self.txtb[reg_name].setText('{0:4.3f} ms '.format(ff_value*proc_period))
          elif(reg_name == 'CTRL_MIN_TRIG_PERIOD'):
            self.txtb[reg_name].setText('0x{0:x} ({0:d} ms) '.format(ff_value, ff_value))
          elif(reg_name == 'CTRL_MAX_BEAMPULSE_WIDTH'):
            self.txtb[reg_name].setText('0x{0:x} ({0:d} us) '.format(ff_value, ff_value))
          else:
            self.txtb[reg_name].setText('0x{0:x}'.format(ff_value))
            if(reg_name == 'CLK_TRIG_MPS_ALARM_HOLD'):  
              for inx in range(5):
                  if(ff_value & (1<<inx)):
                     self.hal[inx].setStyleSheet('background-color: red')
                  else:
                     self.hal[inx].setStyleSheet('background-color: black')
            if(reg_name == 'CLK_TRIG_MPS_ALARM_FIRST'):  
              for inx in range(5):
                  if(ff_value & (1<<inx)):
                     self.fal[inx].setStyleSheet('background-color: red')
                  else:
                     self.fal[inx].setStyleSheet('background-color: black')
            if(reg_name == 'CLK_TRIG_MPS_ALARM_DIRECT'):  
              for inx in range(5):
                  if(ff_value & (1<<inx)):
                     self.dal[inx].setStyleSheet('background-color: red')
                  else:
                     self.dal[inx].setStyleSheet('background-color: black')




if __name__ == '__main__':
   print()
   print("****************************************************")
   print(" Test of BCM on SIS8300-KU")
   print("****************************************************")
   print()
   essffw_devices = essffw.find_essffw_devices()
   print("BCM ESSFFW-running devices found: {}".format(essffw_devices))
   if len(essffw_devices) < 1:
     print("Error: No BCM ESSFFW-running device found in system.")
     exit()

   board_under_test = essffw.Device(essffw.SIS8300Drv())
   board_under_test.add_register_bank(bcm.bcm_fw_registers)
   board_under_test.add_register_bank(bcm.bcm_com_registers)
   board_under_test.add_register_bank(bcm.bcm_acct_registers)
   board_under_test.add_register_bank(bcm.bcm_dod_registers)
   board_under_test.add_register_bank(bcm.bcm_diff_registers)
   board_under_test.add_register_bank(bcm.bcm_fiber_registers)
   board_under_test.add_register_bank(bcm.bcm_probe_registers)
   board_under_test.connect(essffw_devices[0])
   board_under_test['CLOCK_SRC'].write(0x0a)
   app = QApplication(sys.argv)
   ex = App()
   sys.exit(app.exec_())
