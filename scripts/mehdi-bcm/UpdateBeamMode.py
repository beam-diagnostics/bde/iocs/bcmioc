import subprocess
import threading
from time import sleep
import essffw
import essbcm
def run():
    oldMode = -1;
    while True:
      sleep(0.072)
      result = subprocess.check_output(["caget", "BCM-EVR0:BeamMode-RB"])
      res = result.split();
      mode = int(res[1]);
      if( oldMode != mode):
        print('Beam Mode : {0}'.format(mode))
        oldMode = mode      

        if(mode == 0):
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['CLK_TRIG_MPS_ALARM_DISABLE'].offset,  0x0)	
            subprocess.check_output(["caput", "BCM-EVR0:DlyGen0-Width-SP",  "0"])
            sleep(1)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_MODE_REG'].offset,  mode)
            #essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_TRIG_SRC'].offset,  0)
            #essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_TRIG'].offset,  0x01)
            #essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_TRIG'].offset,  0)
            #essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_TRIG_SRC'].offset,  0x03)

        if(mode == 2):
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['MPS_ALL_ALARM_DISABLE'].offset,  0x01)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_MODE_REG'].offset,  mode)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['CTRL_MAX_BEAMPULSE_WIDTH'].offset,  10)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['CLK_TRIG_MPS_ALARM_DISABLE'].offset,  0x0)	
            essffw.write_register('/dev/sis8300-5', essbcm.adc_registers['ADC_UPPER_THRESHOLD'].offset+0x500,  0x5d70)
            essffw.write_register('/dev/sis8300-5', essbcm.adc_registers['ADC_LOWER_THRESHOLD'].offset+0x500,  0x5640)	
            sleep(1)
            subprocess.check_output(["caput", "BCM-EVR0:DlyGen0-Width-SP",  "5"])
            sleep(1)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['MPS_ALL_ALARM_DISABLE'].offset,  0x0)

        if(mode == 8):
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['MPS_ALL_ALARM_DISABLE'].offset,  0x01)

            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_MODE_REG'].offset,  mode)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['CTRL_MAX_BEAMPULSE_WIDTH'].offset,  2870)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['CLK_TRIG_MPS_ALARM_DISABLE'].offset,  0x0)	
            essffw.write_register('/dev/sis8300-5', essbcm.adc_registers['ADC_UPPER_THRESHOLD'].offset+0x500,  0x5d70)
            essffw.write_register('/dev/sis8300-5', essbcm.adc_registers['ADC_LOWER_THRESHOLD'].offset+0x500,  0x5640)	
            sleep(1)
            subprocess.check_output(["caput", "BCM-EVR0:DlyGen0-Width-SP",  "2860"])
            sleep(1)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['MPS_ALL_ALARM_DISABLE'].offset,  0x0)

        if (mode == 7 ):
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['MPS_ALL_ALARM_DISABLE'].offset,  0x01)

            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['BEAM_MODE_REG'].offset,  mode)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['CTRL_MAX_BEAMPULSE_WIDTH'].offset,  2870)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['CLK_TRIG_MPS_ALARM_DISABLE'].offset,  0x0)
            essffw.write_register('/dev/sis8300-5', essbcm.adc_registers['ADC_UPPER_THRESHOLD'].offset+0x500,  0x1fa0)
            essffw.write_register('/dev/sis8300-5', essbcm.adc_registers['ADC_LOWER_THRESHOLD'].offset+0x500,  0x19e0)	
            sleep(1)
            subprocess.check_output(["caput", "BCM-EVR0:DlyGen0-Width-SP",  "2860"])
            sleep(1)
            essffw.write_register('/dev/sis8300-5', essbcm.bcm_common_registers['MPS_ALL_ALARM_DISABLE'].offset,  0x0)


t = threading.Thread(target=run)
t.start()
